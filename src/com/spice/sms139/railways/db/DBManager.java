package com.spice.sms139.railways.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import com.spice.sms139.report.config.ConfigurationManager;

public class DBManager {

	public static Connection objConnection = null;

	public static Connection getConnection() {

		try {
			if (objConnection == null || objConnection.isClosed()) {

				ConfigurationManager cm = ConfigurationManager.getInstance();
				Properties p = cm.getProperties();

				Class.forName(p.getProperty("database.jdbcDriver"));
				objConnection = DriverManager.getConnection(p.getProperty("database.jdbcUrl"),
						p.getProperty("database.username"), p.getProperty("database.password"));

				System.out.println("----------------------------------------------------------------");
				System.out.println("-----------------Connected with  DataBase  ---------------------");
				System.out.println("----------------------------------------------------------------");

			}
		} catch (Exception objException) {
			objException.printStackTrace();
		}
		return objConnection;
	}

}
