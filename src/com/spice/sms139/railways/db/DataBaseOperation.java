package com.spice.sms139.railways.db;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.IOUtils;

import com.spice.sms139.utilities.LogStackTrace;
import com.spice.sms139.utilities.Logs;
import com.spice.sms139.utilities.OsType;

public class DataBaseOperation {

	public String login(String strUserName, String strPassword) {
		String retVal = "";
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		ResultSet rs = null;
		try {
			String strQuery = "SELECT uniqueId FROM tbl_http_accounts where username=? and password=?";
			objConnection = DBManager.getConnection();
			objStatement = objConnection.prepareStatement(strQuery);
			objStatement.setString(1, strUserName);
			objStatement.setString(2, strPassword);
			rs = objStatement.executeQuery();
			if (rs.next()) {
				retVal = rs.getString(1);
			}
		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			DbUtils.closeQuietly(rs);
			// DbUtils.closeQuietly(objConnection);
		}
		return retVal;
	}

	public String getStnCode(String strStnName) {
		String strStnCode = "";
		HashMap<String, String> hm = new HashMap<String, String>();
		FileInputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		try {

			String filePath = "/home/sms139_services/getStnCode.cfg";
			if (OsType.isWindows()) {
				filePath = "E:/sms139_services/getStnCode.cfg";
			}

			fstream = new FileInputStream(filePath);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				String[] parts = strLine.split("#");
				hm.put(parts[0], parts[1]);
			}
			strStnCode = hm.get(strStnName);
		} catch (Exception e) {// Catch exception if any
			Logs.getErrorLogs(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(fstream);
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(br);
		}
		if (strStnCode == null) {
			strStnCode = "OTHER";
		}
		return strStnCode;
	}

	public String checkStnCode(String strStnName) {
		String strStnCode = "";
		HashMap<String, String> hm = new HashMap<String, String>();
		FileInputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		try {
			String filePath = "/home/sms139_services/checkStnCode.cfg";
			if (OsType.isWindows()) {
				filePath = "E:/sms139_services/checkStnCode.cfg";
			}

			fstream = new FileInputStream(filePath);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				String[] parts = strLine.split("#");
				hm.put(parts[0], parts[1]);
			}
			strStnCode = hm.get(strStnName);
		} catch (Exception e) {// Catch exception if any
			Logs.getErrorLogs(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(fstream);
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(br);
		}
		if (strStnCode == null) {
			strStnCode = "OTHER";
		}
		return strStnCode;
	}

	public void insertTrainBetweenTwoStnInfo(String strId, String strMobile, String strMessage, String strResponse,
			int strTotalTrains) {
		System.out.println("Response message passed to DB === " + strResponse);
		strMessage = strMessage.replaceAll("'", "");
		strResponse = strResponse.replaceAll("'", "");
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();
			String strQuery = "INSERT INTO tbl_train_two_station(date_time,message_id,mobile,req_message,resp_message,total_trains,"
					+ "sms_sent,STATUS) VALUES (now(),?,?,?,?,?,?,?)";
			objStatement = objConnection.prepareStatement(strQuery);
			objStatement.setString(1, strId);
			objStatement.setString(2, strMobile);
			objStatement.setString(3, strMessage);
			objStatement.setString(4, strResponse);
			objStatement.setInt(5, strTotalTrains);
			objStatement.setString(6, "1");
			objStatement.setString(7, "A");
			objStatement.executeUpdate();
		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);
		}
	}

	public String getErrorDesc(String strErrorCode, String strErrorDesc) {
		String strDescription = "";
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		ResultSet rs = null;

		try {
			objConnection = DBManager.getConnection();

			String strQuery = "select spice_desc from tbl_irctc_errorcode_desc where error_Code=?";
			objStatement = objConnection.prepareStatement(strQuery);
			objStatement.setString(1, strErrorCode);
			rs = objStatement.executeQuery();
			while (rs.next()) {
				strDescription = rs.getString(1);
			}

			if (strDescription == null || strDescription.equalsIgnoreCase("")) {
				strDescription = strErrorDesc;
			}
		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);
			DbUtils.closeQuietly(rs);
		}
		return strDescription;
	}

	public void updateIRCTCTxnID(String strIRCTCTxnID, String strTID, String strMobile) {
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();
			String strUpdateQuery = "update tbl_Booking_req set IRCTC_Txn_ID=?,IRCTC_URL_call_time=now() where TID=? and mobile=?";

			objStatement = objConnection.prepareStatement(strUpdateQuery);
			objStatement.setString(1, strIRCTCTxnID);
			objStatement.setString(2, strTID);
			objStatement.setString(3, strMobile);
			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);

		}
	}

	public String checkKeyword(String strId, String strUniqueID, String strMsisdn, String strMessage) {

		String strKeyword = "";
		String strResponse = "";
		String strReqKeyword = "";
		if (strMessage != null && strMessage.indexOf(" ") != -1) {
			strReqKeyword = strMessage.substring(0, strMessage.indexOf(" "));
		} else {
			strReqKeyword = strMessage;
		}
		// String strReqKeyword = strMessage.substring(0, strMessage.indexOf("
		// "));
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		ResultSet rs = null;

		try {
			objConnection = DBManager.getConnection();
			String strQuery = "SELECT keyword,resp_message from tbl_sms_139_keywords where status=?";

			objStatement = objConnection.prepareStatement(strQuery);
			objStatement.setString(1, "A");

			rs = objStatement.executeQuery();
			while (rs.next()) {
				strKeyword = rs.getString(1);
				// if (strReqKeyword != null &&
				// strReqKeyword.startsWith(strKeyword)){
				if (strReqKeyword != null && strReqKeyword.equalsIgnoreCase(strKeyword)) {
					strResponse = rs.getString(2);
					String strQuery_insert = "INSERT INTO tbl_sms_139_req_response values (now(),?,?,?,?,?,?,?)";
					objStatement = objConnection.prepareStatement(strQuery_insert);
					objStatement.setString(1, strId);
					objStatement.setString(2, strUniqueID);
					objStatement.setString(3, strMessage);
					objStatement.setString(4, strMsisdn);
					objStatement.setString(5, strResponse);
					objStatement.setString(6, strKeyword);
					objStatement.setString(7, "1");
					objStatement.executeUpdate();
					// insert to Follow request
				}
			}

		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);
			DbUtils.closeQuietly(rs);

		}
		return strResponse;
	}

	public void insertBookingReq(String strId, String strUniqueID, String strMsisdn, String strMessage,
			String Resp_mssage, String train_no, String Source_stn_code, String Dest_stn_code, String DOJ,
			String strclass, int Total_Passenger, String Pass1_Name, int Pass1_Age, String Pass1_Sex, String Pass2_Name,
			int Pass2_Age, String Pass2_Sex, String Pass3_Name, int Pass3_Age, String Pass3_Sex, String pass4_Name,
			int Pass4_Age, String Pass4_Sex, String Pass5_Name, int Pass5_Age, String Pass5_Sex, String Pass6_Name,
			int Pass6_Age, String Pass6_Sex, long TID) {
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();
			String strInsertQuery = "insert into tbl_Booking_req (date_time,message_id,mobile,identifier,req_mssage,Resp_mssage,train_no,Source_stn_code,Dest_stn_code,DOJ,class,Total_Passenger,Pass1_Name,Pass1_Age,Pass1_Sex,Pass2_Name,Pass2_Age,Pass2_Sex,Pass3_Name,Pass3_Age,Pass3_Sex,pass4_Name,Pass4_Age,Pass4_Sex,Pass5_Name,Pass5_Age,Pass5_Sex,Pass6_Name,Pass6_Age,Pass6_Sex,TID) values (now(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			objStatement = objConnection.prepareStatement(strInsertQuery);
			objStatement.setString(1, strId);
			objStatement.setString(2, strMsisdn);
			objStatement.setString(3, strUniqueID);
			objStatement.setString(4, strMessage);
			objStatement.setString(5, Resp_mssage);
			objStatement.setString(6, train_no);
			objStatement.setString(7, Source_stn_code);
			objStatement.setString(8, Dest_stn_code);
			objStatement.setString(9, DOJ);
			objStatement.setString(10, strclass);
			objStatement.setInt(11, Total_Passenger);
			objStatement.setString(12, Pass1_Name);
			objStatement.setInt(13, Pass1_Age);
			objStatement.setString(14, Pass1_Sex);
			objStatement.setString(15, Pass2_Name);
			objStatement.setInt(16, Pass2_Age);
			objStatement.setString(17, Pass2_Sex);
			objStatement.setString(18, Pass3_Name);
			objStatement.setInt(19, Pass3_Age);
			objStatement.setString(20, Pass3_Sex);
			objStatement.setString(21, pass4_Name);
			objStatement.setInt(22, Pass4_Age);
			objStatement.setString(23, Pass4_Sex);
			objStatement.setString(24, Pass5_Name);
			objStatement.setInt(25, Pass5_Age);
			objStatement.setString(26, Pass5_Sex);
			objStatement.setString(27, Pass6_Name);
			objStatement.setInt(28, Pass6_Age);
			objStatement.setString(29, Pass6_Sex);
			objStatement.setLong(30, TID);

			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);

		}

	}

	public void sendFollowSrvcMsg(String strMsisdn) {
		int smsDataDeltSts = 0;
		String strOutPut = "To get updated PNR status automatically;\nType FOLLOW [10 digit PNR Number] and send to 139.\nExample: FOLLOW 6161370784";
		CallableStatement call = null;
		Connection con = null;
		try {
			con = DBManager.getConnection();
			System.out.println("Sending Message....");
			call = con.prepareCall("{call PROC_HANDLE_LONG_MESSAGE_1(?,?,?,?,?,?,?,?)}");
			call.setString(1, strMsisdn);
			call.setString(2, "139");
			call.setString(3, strOutPut);
			call.setString(4, "29");
			call.setString(5, "FLO");
			call.setString(6, "txt");
			call.setString(7, "0");
			call.registerOutParameter(8, Types.VARCHAR);
			call.execute();

			System.out.println("[ FOLLOW PNR ] Processed SMS inserted in sender to deliver! ");
		} catch (Exception e) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(e));
			System.out.println("[ ERROR ] Exception occured while moving processed SMS in repository!" + e);
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(call);
			// DbUtils.closeQuietly(con);
		}
		System.out.println("[ SMS139 ] Returning status with " + smsDataDeltSts);

	}

	public void insertLog(String strId, String strUniqueId, String strMsisdn, String strMessage) {

		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();

			String strInsertQuery = "insert into tbl_tracking_logs (message_id,identifier,mobile,request_message,client_req_time) values (?,?,?,?,now())";
			objStatement = objConnection.prepareStatement(strInsertQuery);
			objStatement.setString(1, strId);
			objStatement.setString(2, strUniqueId);
			objStatement.setString(3, strMsisdn);
			objStatement.setString(4, strMessage);
			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);
		}
	}

	public void updateBBPOurlCall(String strId) {
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();

			String strInsertQuery = "update tbl_tracking_logs set BBPO_url_call_time=now() where message_id=?";
			objStatement = objConnection.prepareStatement(strInsertQuery);
			objStatement.setString(1, strId);
			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);
		}
	}

	public void updateBBPOresp(String strId, String resp_message, String keyword) {

		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();

			String strInsertQuery = "update tbl_tracking_logs set BBPO_resp_time=now(),response_message=?,keyword=? where message_id=?";
			objStatement = objConnection.prepareStatement(strInsertQuery);
			objStatement.setString(1, resp_message);
			objStatement.setString(2, keyword);
			objStatement.setString(3, strId);
			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);
		}
	}

	public void insertRequestLog(String strDateTime, String strId, String strUniqueId, String strMsisdn,
			String strMessage) {
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();

			if (strMessage.startsWith("PAY") || strMessage.startsWith("PT ")) {
				strMessage = strMessage.replaceAll("\\s+", " ");
				String[] parts = strMessage.split("\\s");

				List<String> trimmedParts = new ArrayList<String>();

				for (String part : parts) {
					if (part.trim().length() > 0) {
						trimmedParts.add(part);
					}
				}
				if (trimmedParts.size() > 3) {
					String keyword = trimmedParts.get(0);
					String TID = trimmedParts.get(1);
					String mode = trimmedParts.get(2);
					strMessage = keyword + " " + TID + " " + mode + " XXXX XXXXX XXXXX";
				}
			} else if (strMessage.startsWith("CAN") || strMessage.startsWith("CT ")) {
				strMessage = strMessage.replaceAll("\\s+", " ");
				String[] parts = strMessage.split("\\s");

				List<String> trimmedParts = new ArrayList<String>();

				for (String part : parts) {
					if (part.trim().length() > 0) {
						trimmedParts.add(part);
					}
				}
				if (trimmedParts.size() > 2) {
					String keyword = trimmedParts.get(0);
					String PNR = trimmedParts.get(1);
					strMessage = keyword + " " + PNR + " XXXXXXXXX";
				}
			}
			String strInsertQuery = "insert into tbl_http_request_log(date_time,service_type,message_id,identifier,mobile,source,"
					+ "message,keyword) VALUES(NOW(),?,?,?,?,?,?,?)";
			objStatement = objConnection.prepareStatement(strInsertQuery);
			objStatement.setString(1, "HTTP");
			objStatement.setString(2, strId);
			objStatement.setString(3, strUniqueId);
			objStatement.setString(4, strMsisdn);
			objStatement.setString(5, "139");
			objStatement.setString(6, strMessage);
			objStatement.setString(7, "NA");
			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);
		}

	}

	public void insertFailedreq(String strDateTime, String strId, String strUniqueId, String strMsisdn,
			String strMessage, String keyword) {

		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();

			String strInsertQuery = "insert into tbl_http_momt_failed values (now(),?,?,?,?,?,?,?,?)";

			objStatement = objConnection.prepareStatement(strInsertQuery);
			objStatement.setString(1, "HTPP");
			objStatement.setString(2, strId);
			objStatement.setString(3, strUniqueId);
			objStatement.setString(4, strMsisdn);
			objStatement.setString(5, "139");
			objStatement.setString(6, strMessage);
			objStatement.setString(7, keyword);
			objStatement.setString(8, "1");

			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);
		}

	}

	public void insertResponsetLog(String strId, String strUniqueId, String strMsisdn, String strOutPut,
			String keyword) {
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();
			String strInsertQuery = "insert into tbl_http_response_log(date_time,service_type,message_id,identifier,mobile,"
					+ "source,message,keyword) values(now(),?,?,?,?,?,?,?)";
			objStatement = objConnection.prepareStatement(strInsertQuery);
			objStatement.setString(1, "HTTP");
			objStatement.setString(2, strId);
			objStatement.setString(3, strUniqueId);
			objStatement.setString(4, strMsisdn);
			objStatement.setString(5, "139");
			objStatement.setString(6, strOutPut);
			objStatement.setString(7, keyword);
			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(strId + "," + strUniqueId + "," + LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);
		}

	}

	public static void updateRecordForRetry(String strId, Integer retryCount) {

		Connection objConnection = null;
		PreparedStatement objStatement = null;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, 5);
		try {
			objConnection = DBManager.getConnection();

			String strUpdateQuery = "update tbl_http_request set status=?,retry_time = ?,retry_count=? where id = ?";
			objStatement = objConnection.prepareStatement(strUpdateQuery);
			objStatement.setString(1, "2");
			objStatement.setTimestamp(2, new Timestamp(cal.getTimeInMillis()));
			objStatement.setInt(3, retryCount + 1);
			objStatement.setString(4, strId);
			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			// DbUtils.closeQuietly(objConnection);
		}
	}

}