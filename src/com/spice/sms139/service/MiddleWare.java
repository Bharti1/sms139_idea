package com.spice.sms139.service;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.dbutils.DbUtils;

import com.spice.sms139.commons.CallBooking;
import com.spice.sms139.commons.FormatInputString;
import com.spice.sms139.commons.GetFooter;
import com.spice.sms139.commons.Response;
import com.spice.sms139.railways.db.DBManager;
import com.spice.sms139.railways.db.DataBaseOperation;
import com.spice.sms139.utilities.LogStackTrace;
import com.spice.sms139.utilities.Logs;
import com.spice.sms139.utilities.Utility;

import one39.MTPush;

public class MiddleWare implements Runnable {

	String strMsisdn, strSourceAdderss, strMessage, strUniqueId, strDateTime, strKeyword, strUdh, strDcs, strVmn,
			strImsi, strCircleId;
	String strOutPut = "", strUserName, strPassword, strId;
	Integer retryCount = 0;
	String keyword = "Wrong Message";
	String ResponseUrl = "";
	String strErrorMessage = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
			+ "currently not available. Please try after sometime.";
	String strCorrectFooterMessage = "For help on SMS Based Ticket Booking, SMS TKT to 139";
	String strErrorFooterMessage = "Also get PNR Status and all Train Info on http://getjar.com/railandentertainment";

	MiddleWare(String strId, String strMsisdn, String strSourceAdderss, String strMessage, String strUniqueId,
			String strDateTime, String strKeyword, String strUdh, String strDcs, String strVmn, String strImsi,
			String strCircleId, String strUserName, String strPassword, int retryCount) {
		this.strId = trim(strId);
		this.strMsisdn = trim(strMsisdn);
		this.strSourceAdderss = trim(strSourceAdderss);
		this.strMessage = strMessage;
		this.strUniqueId = strUniqueId;
		this.strDateTime = strDateTime;
		this.strKeyword = strKeyword;
		this.strUdh = strUdh;
		this.strDcs = strDcs;
		this.strVmn = strVmn;
		this.strImsi = strImsi;
		this.strCircleId = strCircleId;
		this.strUserName = strUserName;
		this.strPassword = strPassword;
		this.retryCount = retryCount;
		new Thread(this).start();
	}

	private volatile boolean running = true;

	public void terminate() {
		running = false;
	}

	public void run() {
		while (running) {
			try {
				String strRespMessage = "";
				String strRespMessage_uninor = "";
				String strOrgMessage = "";
				strOrgMessage = strMessage;
				strMessage = strMessage.toUpperCase();

				if (strMessage.startsWith("FOLLOW")) {
					System.out.println("This is from FOllow account.... Ignore for checking..... =  " + strUniqueId);

				} else {
					strRespMessage = checkKeyword(strId, strUniqueId, strMsisdn, strMessage);
				}
				strMessage = trim(getMessage(strMessage));
				FormatInputString formatObj = new FormatInputString();
				GetFooter footerText = new GetFooter();
				Logs.getRequestLogs("Before formatting-------> HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
						+ strMsisdn + "$" + strMessage);
				strMessage = formatObj.getFormattedInput(strId, strUniqueId, strMsisdn, strOrgMessage);
				System.out.println("Message is ====== " + strMessage);
				if (strMessage.startsWith("REGCODE")) {
					System.out.println("Inside REGCODE keyword !!!!");
					strMessage = strMessage.trim();
					CallBooking userAct = CallBooking.getCallBookingInstance();
					strOutPut = userAct.hitUrlWith3Params(Utility.getUrlValue("user_activation"), strOrgMessage,
							strUniqueId, strMsisdn);
					this.keyword = "User Activation";
					strOutPut = strOutPut.replaceAll("<br>", "\n");
					
				} else if (strMessage.startsWith("REG")) {
					strMessage = strMessage.trim();
					CallBooking userReg = CallBooking.getCallBookingInstance();
					strOutPut = userReg.hitUrlWith3Params(Utility.getUrlValue("user_registration"), strOrgMessage,
							strUniqueId, strMsisdn);
					this.keyword = "User Registration";
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("TPIN")) {
					strMessage = strMessage.trim();
					CallBooking tpinResp = CallBooking.getCallBookingInstance();
					strOutPut = tpinResp.hitUrlWith3Params(Utility.getUrlValue("user_tpin"), strOrgMessage, strUniqueId,
							strMsisdn);
					this.keyword = "TPIN Request";
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.equalsIgnoreCase("MORE")) {
					System.out.println("inside More keyword=====");
					GetNextTrainInfo getTinfo = new GetNextTrainInfo();
					this.keyword = "TBS Enquiry";
					strOutPut = getTinfo.getInformation(strMsisdn, strMessage);

				}
				// New Setup testing from Idea
				else if (strMessage.startsWith("ROUTE")) {
					CallBooking routeResp = CallBooking.getCallBookingInstance();
					this.keyword = "Train Route";
					strOutPut = routeResp.hitUrlWith2Params(Utility.getUrlValue("train_route"), strMessage, strUniqueId,
							strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("SCHEDULE")) {
					CallBooking schResp = CallBooking.getCallBookingInstance();
					this.keyword = "Train Schedule";
					strOutPut = schResp.hitUrlWith2Params(Utility.getUrlValue("train_schedule"), strMessage,
							strUniqueId, strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("TSEAT")) {
					CallBooking tatkalSeatResp = CallBooking.getCallBookingInstance();
					this.keyword = "Tatkal Seat";
					strOutPut = tatkalSeatResp.hitUrlWith2Params(Utility.getUrlValue("tatkal_seat"), strMessage,
							strUniqueId, strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("NEXT")) {
					CallBooking nextTrainResp = CallBooking.getCallBookingInstance();
					this.keyword = "Next Train";
					strOutPut = nextTrainResp.hitUrlWith2Params(Utility.getUrlValue("next_train"), strMessage,
							strUniqueId, strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("TRAIN")) {
					if (strOutPut.indexOf(" ") != -1) {
						String[] parts = strMessage.split("\\s");

						List<String> trimmedParts = new ArrayList<String>();
						for (String part : parts) {
							if (part.trim().length() > 0) {
								trimmedParts.add(part);
							}
						}
						if (trimmedParts.size() == 2) {
							this.keyword = "TimeTable Enquiry";
						} else if (trimmedParts.size() == 7) {
							this.keyword = "Train Accommodation";
						} else if (trimmedParts.size() < 6 && isCharacters(trimmedParts.get(0))
								&& isCharacters(trimmedParts.get(1)) && isCharacters(trimmedParts.get(2))) {
							this.keyword = "TBS Enquiry";
						} else if (trimmedParts.size() == 4) {
							this.keyword = "Train Enquiry";
						} else {
							this.keyword = "TBS Enquiry";
						}
					} else {
						this.keyword = "TBS Enquiry";
					}
					CallBooking getStnCodeResp = CallBooking.getCallBookingInstance();
					strOutPut = getStnCodeResp.hitUrlWith2Params(Utility.getUrlValue("stn_code"), strMessage,
							strUniqueId, strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("CODE")) {
					CallBooking getStnCodeResp = CallBooking.getCallBookingInstance();
					this.keyword = "CODE Enquiry";
					strOutPut = getStnCodeResp.hitUrlWith2Params(Utility.getUrlValue("stn_code"), strMessage,
							strUniqueId, strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("FARE")) {
					CallBooking getFareEnq = CallBooking.getCallBookingInstance();
					this.keyword = "Fair Enquiry";
					strOutPut = getFareEnq.hitUrlWith2Params(Utility.getUrlValue("fare_info"), strMessage, strUniqueId,
							strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("SEAT")) {
					CallBooking getSeatEnq = CallBooking.getCallBookingInstance();
					this.keyword = "Train Accommodation";
					strOutPut = getSeatEnq.hitUrlWith2Params(Utility.getUrlValue("seat_info"), strMessage, strUniqueId,
							strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("AD")) {
					CallBooking getAdEnq = CallBooking.getCallBookingInstance();
					this.keyword = "Train Enquiry";
					strOutPut = getAdEnq.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
							strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("ALERT")) {
					CallBooking getAdEnq = CallBooking.getCallBookingInstance();
					this.keyword = "Destination Alert";
					strOutPut = getAdEnq.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
							strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("ALARM")) {
					CallBooking getAdEnq = CallBooking.getCallBookingInstance();
					this.keyword = "Destination Alarm";
					strOutPut = getAdEnq.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
							strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("PLATFORM")) {
					CallBooking getAdEnq = CallBooking.getCallBookingInstance();
					this.keyword = "Platform Enquiry";
					strOutPut = getAdEnq.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
							strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("MEAL") || strMessage.startsWith("FOOD")) {
					CallBooking getAdEnq = CallBooking.getCallBookingInstance();
					this.keyword = "Meal Request";
					strOutPut = getAdEnq.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
							strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("TIME")) {
					CallBooking getAdEnq = CallBooking.getCallBookingInstance();
					this.keyword = "TimeTable Enquiry";
					strOutPut = getAdEnq.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
							strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("TN")) {
					CallBooking getAdEnq = CallBooking.getCallBookingInstance();
					this.keyword = "TN Enquiry";
					strOutPut = getAdEnq.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
							strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("SPOT")) {
					CallBooking spotEnq = CallBooking.getCallBookingInstance();
					this.keyword = "SPOT Train";
					strOutPut = spotEnq.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
							strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				}

				else if (strMessage.startsWith("CANCEL")) {
					strMessage = strMessage.trim();
					CallBooking BookHelp = CallBooking.getCallBookingInstance();
					strOutPut = BookHelp.hitUrlWith2Params(Utility.getUrlValue("booking_cancel"), strMessage,
							strUniqueId, strMsisdn);
					this.keyword = "CANCEL TKT";
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				}

				else if (strMessage.startsWith("OTP")) {
					strMessage = strMessage.trim();
					CallBooking BookHelp = CallBooking.getCallBookingInstance();
					strOutPut = BookHelp.hitUrlWith2Params(Utility.getUrlValue("otp_booking_cancel"), strMessage,
							strUniqueId, strMsisdn);
					this.keyword = "OTP 4 CANCEL";
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				}

				else if (strMessage.startsWith("BOOKTA") || strMessage.startsWith("BOOKUA")
						|| strMessage.startsWith("BOOKSA") || strMessage.startsWith("BOOKSLA")
						|| strMessage.startsWith("BOOKSPA") || strMessage.startsWith("BOOKBAN")
						|| strMessage.startsWith("BOOKBJT")) {
					strMessage = strMessage.trim();
					CallBooking BookResp = CallBooking.getCallBookingInstance();
					strOutPut = BookResp.getBookingResponseTest(strMessage, strUniqueId, strMsisdn);
					this.keyword = "BOOK Request";
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				}

				else if (strMessage.startsWith("BOOKKA")) {
					strMessage = strMessage.trim();
					CallBooking BookResp = CallBooking.getCallBookingInstance();
					strOutPut = BookResp.hitUrlWith3Params(Utility.getUrlValue("booking_child_berth"), strMessage,
							strUniqueId, strMsisdn);
					this.keyword = "BOOK Request";
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				}

				else if (strMessage.startsWith("PAYTA") || strMessage.startsWith("PAYSA")
						|| strMessage.startsWith("PAYSPA") || strMessage.startsWith("PAYSLA")
						|| strMessage.startsWith("PAYBAN") || strMessage.startsWith("PAYBJT")) {
					strMessage = strMessage.trim();
					CallBooking payResp = CallBooking.getCallBookingInstance();
					strOutPut = payResp.hitUrlWith3Params(Utility.getUrlValue("payment_response"), strMessage,
							strUniqueId, strMsisdn);
					this.keyword = "BOOK Ticket";
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("BOOK")) {
					strMessage = strMessage.trim();
					CallBooking BookResp = CallBooking.getCallBookingInstance();
					// strOutPut =
					// BookResp.hitUrlWith3Params(Utility.getUrlValue("booking_response"),
					// strMessage,
					// strUniqueId, strMsisdn);
					this.keyword = "BOOK Request";
					// strOutPut = strOutPut.replaceAll("<br>", "\n");
					strOutPut = "Dear User, SMS based Ticket Booking facility has been stopped by Indian Railways. Inconvenience caused is deeply regretted.";

				} else if (strMessage.startsWith("PAY")) {
					strMessage = strMessage.trim();
					CallBooking PayResp = CallBooking.getCallBookingInstance();
					// strOutPut =
					// PayResp.hitUrlWith3Params(Utility.getUrlValue("payment_response"),
					// strOrgMessage,
					// strUniqueId, strMsisdn);
					this.keyword = "BOOK Ticket";
					// strOutPut = strOutPut.replaceAll("<br>", "\n");
					strOutPut = "Dear User, SMS based Ticket Booking facility has been stopped by Indian Railways. Inconvenience caused is deeply regretted.";

				} else if (strMessage.startsWith("CAN")) {
					strMessage = strMessage.trim();
					CallBooking CanReq = CallBooking.getCallBookingInstance();
					strOutPut = CanReq.hitUrlWith3Params(Utility.getUrlValue("cancel_request"), strOrgMessage,
							strUniqueId, strMsisdn);
					this.keyword = "Cancel Request";
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("YES")) {
					strMessage = strMessage.trim();
					CallBooking ConfirmCan = CallBooking.getCallBookingInstance();
					strOutPut = ConfirmCan.hitUrlWith3Params(Utility.getUrlValue("confirm_cancellation"), strOrgMessage,
							strUniqueId, strMsisdn);
					this.keyword = "Cancel Ticket";
					strOutPut = strOutPut.replaceAll("<br>", "\n");

				} else if (strMessage.startsWith("TRANS")) {
					strMessage = strMessage.trim();
					CallBooking TransStatus = CallBooking.getCallBookingInstance();
					strOutPut = TransStatus.hitUrlWith3Params(Utility.getUrlValue("transaction_status"), strMessage,
							strUniqueId, strMsisdn);
					strOutPut = strOutPut.replaceAll("<br>", "\n");
					System.out.println("Formatted String == " + strOutPut);
					this.keyword = "Transaction Status";
				}

				else if (strMessage.equalsIgnoreCase("TKT")) {
					strMessage = strMessage.trim();
					CallBooking BookHelp = CallBooking.getCallBookingInstance();
					strOutPut = BookHelp.hitUrlWith3Params(Utility.getUrlValue("booking_help"), strOrgMessage,
							strUniqueId, strMsisdn);
					this.keyword = "Ticket Help";
					strOutPut = strOutPut.replaceAll("<br>", "\n");
				} else if (strRespMessage != null && strRespMessage != "") {
					strOutPut = strRespMessage;
					this.keyword = "Response Message";
					strOutPut = strOutPut.replaceAll("#", ",");
					strOutPut = getComment(strOutPut);
				} else if (strMessage.startsWith("FOLLOW")) {
					strOutPut = "To get PNR status, please send PNR [10 digit PNR Number] to 139. e.g PNR 1234567890.";
					this.keyword = "FOLLOW PNR";

				} else if (strMessage.startsWith("APP") || strMessage.startsWith("AAP")
						|| strMessage.startsWith("ABP")) {
					strOutPut = "Click http://bit.ly/ticketingapp to Download SMS Based Railway Ticket Mobile Booking & Enquiry Application. You can also download the application for Android phones from Play Store or for JAVA/J2ME phones from Get Jar.";
					this.keyword = "APP Download";

				} else if (strMessage.startsWith("IRCTCID")) {
					strOutPut = "Dear User, now you can create IRCTC Login ID from your mobile in 2 steps without Internet & Computer. You can use this ID to book tickets from Mobile. To create Login ID, please SMS REG (First Name) (Last Name) (Date of Birth DDMMYY). Example : REG Sandeep Kumar 110284. Note that the mobile number must not be already registered with IRCTC.";
					this.keyword = "Help Message";

				} else if (strMessage.startsWith("LOUNGE")) {
					strOutPut = "Enjoy world class hospitality in IRCTC Executive Lounge, at Platform No. 16, New Delhi Railway Station. Charges Rs. 350 for 3 Hours. Services: Food & Beverages, Wi-Fi, Recliners, Toilet, TV etc. Extra hour at Rs. 150/hour. Additional services of Wash and Change, Business Center and Convenience Store are also available on additional charges.For Bookings & more details Contact at: +919971992006 or 011-23213487. Book online at www.irctc.co.in or www.railtourismindia.com. ";
					this.keyword = "LOUNGE";

				}else if(strMessage.toLowerCase().startsWith("ob")){
					strMessage = strMessage.trim();
					CallBooking TransStat = CallBooking.getCallBookingInstance();
					this.keyword = "Coach Mitra";
					//String pnr=getPnrNUmber(strMsisdn);
					System.out.println("OB msg is "+strMessage);
					strOutPut = TransStat.hitUrlWith2Params(Utility.getUrlValue("coach_mitra"), strMsisdn
							, "",strMessage);
					System.out.println("OB url is hitted::::::::::::"+ strOutPut);
					strOutPut = strOutPut.replaceAll("<br>", "\n");
				}else if(strMessage.toUpperCase().startsWith("MADAD") || strMessage.toUpperCase().startsWith("MADED") || 
						strMessage.toUpperCase().startsWith("MADDAD") || strMessage.toUpperCase().startsWith("MADAT") || strMessage.toUpperCase().startsWith("MADDAT")){
					strMessage = strMessage.trim();
					CallBooking TransStat = CallBooking.getCallBookingInstance();
					this.keyword = "MADAD";
					System.out.println("MADAD msg is "+strMessage);
					Logs.getRequestLogs("MADAD msg is "+strMessage);
					strOutPut = TransStat.hitUrlWith2Params(Utility.getUrlValue("madad"), strMessage
							, "",strMsisdn);
					System.out.println("MADAD url is hitted::::::::::::"+ strOutPut);
					Logs.getRequestLogs("MADAD url is hitted::::::::::::"+ strOutPut);
					strOutPut = strOutPut.replaceAll("<br/>", "");
				}

				else {
					process();
					strOutPut = strOutPut.trim();
					if (strOutPut != null && strOutPut.indexOf("<!DOCTYPE") != -1) {
						strOutPut = strOutPut.substring(0, strOutPut.indexOf("<!DOCTYPE")).trim();
					}
					if (strOutPut.equalsIgnoreCase("") || strOutPut == null) {
						strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
								+ "currently not available. Please try after sometime.";

					}
					strOutPut = getComment(strOutPut);

				}
				Logs.getRequestLogs(
						"HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$" + strMsisdn + "$" + strOutPut);

				if ((strOutPut != null && strOutPut.equalsIgnoreCase("Error"))
						|| (strOutPut != null && strOutPut.indexOf("WebException") != -1)
						|| (strOutPut != null && strOutPut.indexOf("Unable to connect to the remote server") != -1)
						|| (strOutPut != null && strOutPut.startsWith("The Data Is Unavailable..."))
						|| (strOutPut != null && strOutPut.indexOf("The operation has timed") != -1)
						|| (strOutPut != null && strOutPut.indexOf("NullReferenceException") != -1)
						|| (strOutPut != null && strOutPut.indexOf("Client found response content type") != -1)
						|| (strOutPut.indexOf("is currently not available") != -1)
						|| (strOutPut.indexOf("unable to process your request") != -1)) {
					strOutPut = strErrorMessage;
					// Logs.getRetryLogs(
					// "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
					// + strMsisdn + "$" + strOutPut);
					// DataBaseOperation.updateRecordForRetry(strId);
					// terminate();
					// RequestProcessor.intCounterThread--;
					// continue;
				}
				strOutPut = strOutPut.replaceAll("Thread was being aborted", "");
				strOutPut = strOutPut.replaceAll("Sourde", "Source");
				strOutPut = strOutPut.replaceAll("REA ", "");

				if (strMessage.startsWith("C ")) {
				}
				// TN keyword handling
				else if (strMessage.startsWith("TN")) {
					if (strOutPut.toLowerCase().indexOf("wrong train name") != -1
							|| strOutPut.toLowerCase().indexOf("invalid train number") != -1) {
						if (strOutPut.contains("ERROR : Invalid Train No")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train No", "");
						} else if (strOutPut.contains("ERROR : Invalid Train Number")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train Number", "");
						}

						else {
							if (validateTrainNo(strMessage)) {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For TRAIN NAME Enquiry, SMS TN [TRAIN NO.] to 139. For e.g.: TN 12012. Sorry for the inconvenience. Wishing you safe journey.";
							} else {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Name. For TRAIN Number Enquiry, SMS TN [TRAIN NAME] to 139. For e.g.: TN SHATABDI EXP. Sorry for the inconvenience. Wishing you safe journey.";
							}
						}
					} else {
						if (strOutPut.toLowerCase().indexOf("rea error") != -1) {
							strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
									+ "currently not available. Please try after sometime.";
						} else {

							strOutPut = strOutPut;
						}
					}
				}

				else if (strMessage.startsWith("PLATFORM")) {
					// Incorrect STD Code
					if (strOutPut.indexOf("provide correct STD Code") != -1
							|| strOutPut.indexOf("Given station does not appear") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Station STD Code. For Platform Number Enquiry, SMS PLATFORM <Train No.> <STD code> to 139. For e.g.: PLATFORM 12012 011. Sorry for the inconvenience.";
					}
					// Incorrect Train number
					else if (strOutPut.indexOf("Please enter valid train number") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For Platform Number Enquiry, SMS PLATFORM <Train No.> <STD code> to 139. For e.g.: PLATFORM 12012 011. Sorry for the inconvenience.";
					}
					// Multiple errors
					else if (strOutPut.equalsIgnoreCase("") || strOutPut.equalsIgnoreCase("<br>")
							|| strOutPut.indexOf("ERROR") != -1) {
						strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
								+ "currently not available. Please try after sometime.";
					} else if (strOutPut.indexOf("invalid syntax") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid syntax For Platform Enquiry, SMS PLATFORM <Train No.> <STD code> to 139. For e.g.: PLATFORM 12012 011. Sorry for the inconvenience.";
					} else {
						strOutPut = strOutPut;
					}
					this.keyword = "Platform Enquiry";
				}
				// PNR enquiry
				else if (keyword.equalsIgnoreCase("Pnr Enquiry")) {
					if (strOutPut.indexOf("Invalid Pnr") != -1 || strOutPut.indexOf("invalid PNR") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid PNR number. For PNR enquiry send PNR [10 digit valid PNR number] to 139. Sorry for the inconvenience. Wishing you safe journey.";
					} else if (strOutPut.indexOf("FetchPNRInformation") != -1 || strOutPut.equals("")) {
						strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is currently not available. Please try after sometime.";
						// Logs.getRetryLogs(
						// "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId +
						// "$" + strMsisdn + "$" + strOutPut);
						// DataBaseOperation.updateRecordForRetry(strId);
						// terminate();
						// RequestProcessor.intCounterThread--;
						// continue;
					} else {
						strOutPut = strOutPut;
					}
				}
				// Train Enquiry
				else if (keyword.equalsIgnoreCase("Train Enquiry")) {
					// Incorrect STD Code
					if (strOutPut.indexOf("provide correct STD Code") != -1
							|| strOutPut.indexOf("Given station does not appear") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Station STD Code. For Train Arrival Departure enquiry, please CALL 139 or SMS AD <Train No.> <STD code> to 139. For e.g.: AD 12012 011. Sorry for the inconvenience.";
					}
					// Incorrect Train number
					else if (strOutPut.indexOf("Please enter valid train number") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For Train Arrival Departure enquiry, please CALL 139 or SMS AD <Train No> <STD code> to 139. For e.g.: AD 12012 011. Sorry for the inconvenience.";
					}
					// Multiple errors
					else if (strOutPut.equalsIgnoreCase("") || strOutPut.equalsIgnoreCase("<br>")
							|| strOutPut.indexOf("ERROR") != -1) {
						strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
								+ "currently not available. Please try after sometime.";
					}

					if (strOutPut.toLowerCase().indexOf("wrong train name") != -1
							|| strOutPut.toLowerCase().indexOf("invalid train") != -1) {

						if (strOutPut.contains("ERROR : Invalid Train No")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train No", "");
						} else if (strOutPut.contains("ERROR : Invalid Train Number")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train Number", "");
						}

						else {
							if (validateTrainNo(strMessage)) {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For TRAIN NAME Enquiry, SMS TN [TRAIN NO.] to 139. For e.g.: TN 12012. Sorry for the inconvenience. Wishing you safe journey.";
							} else {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Name. For TRAIN Number Enquiry, SMS TN [TRAIN NAME] to 139. For e.g.: TN SHATABDI EXP. Sorry for the inconvenience. Wishing you safe journey.";
							}
						}
					} else {
						if (strOutPut.toLowerCase().indexOf("rea error") != -1) {
							strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
									+ "currently not available. Please try after sometime.";
						} /*
							 * else {
							 * 
							 * strOutPut = strOutPut + " " +
							 * footerText.getFooterText(strMessage, "idea"); }
							 */
					}
					strOutPut = strOutPut;
				}
				// Train Accomodation
				else if (keyword.equalsIgnoreCase("Train Accommodation")) {
					// Incorrect Qouta Code
					if (strOutPut.indexOf("correct Quota Code") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Quota Code. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience.  Wishing you safe journey.";
					}
					// Icorrect Class code
					else if (strOutPut.indexOf("Please Enter Valid Class Code") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Class Code. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience.  Wishing you safe journey.";
					}

					// Incorrect station Code
					else if (strOutPut.indexOf("correct STD") != -1 || strOutPut.indexOf("correct Source STD") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Station STD Code. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Incorrect Date.
					else if (strOutPut.indexOf("enter valid date") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Date. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Incorrect Train No.
					else if (strOutPut.indexOf("provide Correct Train") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Train Number. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Multiple Errors
					else if (strOutPut.equalsIgnoreCase("your data is not correct<br>")) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid information. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}

					if ((strOutPut.toLowerCase().indexOf("rea error") != -1)
							|| (strOutPut.toLowerCase().indexOf("error") != -1)) {
						strOutPut = strErrorMessage;
					}
					strOutPut = strOutPut;
				}
				// Fair Enquiry
				else if (keyword.equalsIgnoreCase("Fair Enquiry")) {
					// Incorrect Qouta code
					if (strOutPut.indexOf("correct Quota Code") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Quota Code. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Icorrect Class code
					else if (strOutPut.indexOf("Please Enter Valid Class Code") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Class Code. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Incorrect station Code
					else if (strOutPut.indexOf("correct STD") != -1 || strOutPut.indexOf("correct Source STD") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Station STD Code. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Incorrect Date.
					else if (strOutPut.indexOf("enter valid date") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Date. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Incorrect Train No.
					else if (strOutPut.indexOf("Please enter Valid Train") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Train Number. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Multiple Errors
					else if (strOutPut.indexOf("Sorry Fare information") != -1
							|| strOutPut.indexOf("your data is not correct") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid information. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					} else {
						strOutPut = strOutPut;
					}
				}
				// Time Table Enquiry
				else if (keyword.equalsIgnoreCase("TimeTable Enquiry")) {
					// Invalid Train No.
					if (strOutPut.indexOf("Please Enter valid Train") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For TRAIN TIME TABLE Enquiry, SMS TIME <Train No.> to 139. For e.g.: TIME 12012. Sorry for the inconvenience. Wishing you safe journey.";
					} else {
						strOutPut = strOutPut;
					}
				}
				// SPOT Train
				else if (keyword.equalsIgnoreCase("SPOT Train")) {
					// Incorrect Train number
					if (strOutPut.indexOf("Please Enter valid Train") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For Locating the TRAIN, SMS SPOT <Train No.> to 139. For e.g.: SPOT 12012. Sorry for the inconvenience. Wishing you safe journey.";
					} else if ((strOutPut.toLowerCase().indexOf("no record found") != -1)
							|| (strOutPut.toLowerCase().indexOf("unable to process your request") != -1)) {
						strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
								+ "currently not available. Please try after sometime.";
					}

					if (strOutPut.toLowerCase().indexOf("wrong train name") != -1
							|| strOutPut.toLowerCase().indexOf("invalid train") != -1) {

						if (strOutPut.contains("ERROR : Invalid Train No")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train No", "");
						} else if (strOutPut.contains("ERROR : Invalid Train Number")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train Number", "");
						}

						else {
							if (validateTrainNo(strMessage)) {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For TRAIN NAME Enquiry, SMS TN [TRAIN NO.] to 139. For e.g.: TN 12012. Sorry for the inconvenience. Wishing you safe journey.";
							} else {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Name. For TRAIN Number Enquiry, SMS TN [TRAIN NAME] to 139. For e.g.: TN SHATABDI EXP. Sorry for the inconvenience. Wishing you safe journey.";
							}
						}
					}
					if ((strOutPut.toLowerCase().indexOf("rea error") != -1)
							|| (strOutPut.toLowerCase().indexOf("error") != -1)
							|| (strOutPut.toLowerCase().indexOf("unable to process") != -1)) {
						strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
								+ "currently not available. Please try after sometime.";
					} else {

						strOutPut = strOutPut;
					}
				}
				// Handling wrong keyword message
				else if (keyword.equalsIgnoreCase("Wrong Message")) {
				} else if (keyword.equalsIgnoreCase("BOOK Request") || keyword.equalsIgnoreCase("Cancel Request")
						|| keyword.equalsIgnoreCase("Cancel Ticket") || keyword.equalsIgnoreCase("BOOK Ticket")
						|| keyword.equalsIgnoreCase("Ticket Help")) {
					// strOutPut = strOutPut +
					// footerText.getFooterText(strMessage, "idea");
				} else if (keyword.equalsIgnoreCase("CANCEL TKT") || keyword.equalsIgnoreCase("OTP 4 CANCEL")) {
				}

				if (strOutPut.equalsIgnoreCase("")
						|| (strOutPut.toLowerCase().indexOf("is currently not available") != -1)
						|| (strOutPut.toLowerCase().indexOf("unable to process your request") != -1)
						|| (strOutPut.toLowerCase().indexOf("not available at the moment") != -1)
						|| (strOutPut.toLowerCase().indexOf("is unavailable at this time") != -1)
						|| (strOutPut.toLowerCase().indexOf("no record found") != -1)
						|| (strOutPut.toLowerCase().indexOf("contact administrator") != -1)
						|| (strOutPut.toLowerCase().indexOf("unknown exception") != -1)
						|| (strOutPut.toLowerCase().indexOf("error") != -1)
						|| (strOutPut.toLowerCase().indexOf("no record found") != -1)
						|| (strOutPut.toLowerCase().indexOf("input data is incorrect") != -1)) {
					strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
							+ "currently not available. Please try after sometime."
							+ footerText.getFooterText(strMessage, "idea");
					if (retryCount < 3) {
						DataBaseOperation.updateRecordForRetry(strId, retryCount);
						terminate();
						Logs.getRetryLogs(
								"HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$" + strMsisdn + "$" + strOutPut);
						RequestProcessor.intCounterThread--;
						continue;
					}
				} else {
					strOutPut = strOutPut;
				}
				// NEW code by kamal - BEGIN - 09-03-2016
				if (strOutPut.indexOf(
						"Either PNR Number is not valid or data is not available The Data Is Unavailable") != -1) {
					strOutPut = "You have sent an invalid PNR number.Please enter valid 10 digit number";
				} else if (strOutPut.indexOf("REA ERROR :Please provide correct Station Code") != -1) {
					strOutPut = "Sorry invalid station STD code. Pls enter valid station code";
				} else if (strOutPut.indexOf("available from REA for FetchPNRInformation") != -1) {
					strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
							+ "currently not available. Please try after sometime.";
				} else if (strOutPut.indexOf("Data is not available from REA for NTESTrainNTESInformation") != -1) {
					strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
							+ "currently not available. Please try after sometime.";
				} else if (strOutPut
						.indexOf("Data is not available from REA for FetchTrainBetweenStationsInformations") != -1) {
					strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
							+ "currently not available. Please try after sometime.";
				} else if (strOutPut.toLowerCase().indexOf("rea error ") != -1) {
					strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
							+ "currently not available. Please try after sometime.";
				}

				else if (strOutPut.indexOf("REA ERROR : Please enter Valid Date") != -1
						&& keyword.equalsIgnoreCase("TimeTable Enquiry")) {
					strOutPut = "Sorry Train is not running Today. Please Enter valid Train Number.";
				}

				// NEW code by kamal---- END - 09-03-2016

				System.out.println("final output  message is::::::::::: " + strOutPut);
				String strLogingMessage = strOutPut.replaceAll("\\n", "<br>");
				System.out.println("final output  message is::::::::::: " + strOutPut);

				if (strOutPut.equalsIgnoreCase("")
						|| (strOutPut.toLowerCase().indexOf("is currently not available") != -1)
						|| (strOutPut.toLowerCase().indexOf("unable to process your request") != -1)
						|| (strOutPut.toLowerCase().indexOf("not available at the moment") != -1)
						|| (strOutPut.toLowerCase().indexOf("is unavailable at this time") != -1)
						|| (strOutPut.toLowerCase().indexOf("no record found") != -1)
						|| (strOutPut.toLowerCase().indexOf("contact administrator") != -1)
						|| (strOutPut.toLowerCase().indexOf("unknown exception") != -1)
						|| (strOutPut.toLowerCase().indexOf("error") != -1)
						|| (strOutPut.toLowerCase().indexOf("no record found") != -1)
						|| (strOutPut.toLowerCase().indexOf("input data is incorrect") != -1)) {
					strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
							+ "currently not available. Please try after sometime."
							+ footerText.getFooterText(strMessage, "idea");
					if (retryCount < 3) {
						DataBaseOperation.updateRecordForRetry(strId, retryCount);
						terminate();
						Logs.getRetryLogs(
								"HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$" + strMsisdn + "$" + strOutPut);
						RequestProcessor.intCounterThread--;
						continue;
					}
				} else {
					strOutPut = strOutPut + " " + footerText.getFooterText(strMessage, "idea");
				}

				strLogingMessage=strLogingMessage+ " " + footerText.getFooterText(strMessage, "idea");
				logResponseMessage(strId, strUniqueId, strMsisdn, strLogingMessage);
				strRespMessage_uninor = strOutPut;
				strOutPut = URLEncoder.encode(strOutPut, "UTF-8");

				if ((strUniqueId != null && strUniqueId.equals("ida"))
						|| (strUniqueId != null && strUniqueId.equals("idk"))) {
					strRespMessage_uninor = strRespMessage_uninor.replaceAll("<", "(");
					strRespMessage_uninor = strRespMessage_uninor.replaceAll(">", ")");
					strRespMessage_uninor = strRespMessage_uninor.replaceAll("&", "&amp;");
					MTPush mtPush = new MTPush();
					mtPush.setMsisdn(strMsisdn);
					mtPush.setMessage(strRespMessage_uninor);
					mtPush.setUniqueId(strCircleId);
					mtPush.setMoKeyword("text");
					mtPush.setDestMessageId(strId);
					mtPush.send();
				}
				RequestProcessor.intCounterThread--;
				System.out.println(".................................End above................");
				terminate();
			} catch (Exception ex) {
				ex.printStackTrace();
				terminate();
				RequestProcessor.intCounterThread--;
			}
		}
	}

	/*public synchronized String getPnrNUmber(String strMsisdn) {
		String pnr = "";
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		ResultSet rs = null;
		String msg=null;
		
		try {
			String strQuery = "SELECT message FROM tbl_http_request_log where message like 'OB%' and mobile='"+strMsisdn+"' order by date_time desc";
			objConnection = DBManager.getConnection();
			objStatement = objConnection.prepareStatement(strQuery);
			rs = objStatement.executeQuery();
			if (rs.next()) {
				msg = rs.getString(1);
				if(msg.length() > 10 && msg.matches(".*\\d")){
					pnr=msg.substring(msg.length() - 10);
				}
			}
		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(objConnection);
		}
		return pnr;
	}*/

	public synchronized void logResponseMessage(String strId, String strUniqueId, String strMsisdn, String strOutPut) {
		Connection objConnection = null;
		PreparedStatement updateStatement = null;
		try {

			objConnection = DBManager.getConnection();
			String strInsertQuery = "INSERT INTO tbl_http_response_log(date_time,service_type,message_id,identifier,mobile,source,message,keyword) VALUES (NOW(),?,?,?,?,?,?,?)";
			updateStatement = objConnection.prepareStatement(strInsertQuery);
			updateStatement.setString(1, "HTTP");
			updateStatement.setString(2, strId);
			updateStatement.setString(3, strUniqueId);
			updateStatement.setString(4, strMsisdn);
			updateStatement.setString(5, "139");
			updateStatement.setString(6, strOutPut);
			updateStatement.setString(7, keyword);
			updateStatement.executeUpdate();

			if (strOutPut != null && !strOutPut.equals(strErrorMessage)) {
				updateStatement.executeUpdate("delete from tbl_http_request  where id='" + strId + "'");
			}

		} catch (Exception ex) {
			Logs.getErrorLogs("in logResponseMessage--->" + LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(updateStatement);
			// DbUtils.closeQuietly(objConnection);
		}
	}

	public boolean validateTrainNo(String strMessage) {
		int intMessageLength = strMessage.length();
		boolean stat = false;
		int intMessageCounter = 0;
		String strMessageCharacter = "", strReturnValue = "";
		while (intMessageCounter < intMessageLength) {
			strMessageCharacter = strMessage.charAt(intMessageCounter) + "";
			if (strMessageCharacter.equals("1") || strMessageCharacter.equals("2") || strMessageCharacter.equals("3")
					|| strMessageCharacter.equals("4") || strMessageCharacter.equals("5")
					|| strMessageCharacter.equals("6") || strMessageCharacter.equals("7")
					|| strMessageCharacter.equals("8") || strMessageCharacter.equals("9")
					|| strMessageCharacter.equals("0")) {
				strReturnValue += strMessageCharacter;
			}
			intMessageCounter++;
		}
		if (strReturnValue.length() > 4) {
			stat = true;
		} else {
			stat = false;
		}

		return stat;
	}

	public String checkKeyword(String strId, String strUniqueId, String strMsisdn, String strMessage) {
		String strKeyword = "";
		String strResponse = "";
		String strReqKeyword = "";
		if (strMessage != null && strMessage.indexOf(" ") != -1) {
			strReqKeyword = strMessage.substring(0, strMessage.indexOf(" "));
		} else {
			strReqKeyword = strMessage;
		}
		Connection objConnection = null;
		PreparedStatement objStatement = null, insertStatement = null;
		ResultSet rs = null;
		try {
			objConnection = DBManager.getConnection();
			String strQuery = "SELECT keyword,resp_message from tbl_sms_139_keywords where status=?";
			objStatement = objConnection.prepareStatement(strQuery);
			objStatement.setString(1, "A");
			rs = objStatement.executeQuery();

			String insertQuery = "INSERT INTO tbl_sms_139_req_response(date_time,message_id,identifier,req_message,mobile,resp_message,keyword,STATUS) VALUES (NOW(),?,?,?,?,?,?,?)";
			insertStatement = objConnection.prepareStatement(insertQuery);
			while (rs.next()) {
				strKeyword = rs.getString(1);
				if (strReqKeyword != null && strReqKeyword.equalsIgnoreCase(strKeyword)) {
					strResponse = rs.getString(2);

					insertStatement.setString(1, strId);
					insertStatement.setString(2, strUniqueId);
					insertStatement.setString(3, strMessage);
					insertStatement.setString(4, strMsisdn);
					insertStatement.setString(5, strResponse);
					insertStatement.setString(6, strKeyword);
					insertStatement.setString(7, "1");
					insertStatement.executeUpdate();
					break;
				}
			}

		} catch (Exception ex) {
			Logs.getErrorLogs("in checkKeyword--->" + LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(objStatement);
			DbUtils.closeQuietly(insertStatement);
			// DbUtils.closeQuietly(objConnection);
		}
		return strResponse;
	}

	public void process() {
		try {
			MakeURL objMakeURL = new MakeURL();
			Response objResponse = new Response();
			if (strMessage.equalsIgnoreCase("help") || strMessage.equalsIgnoreCase("hlp")
					|| strMessage.equalsIgnoreCase("Menu") || strMessage.equalsIgnoreCase("hel")
					|| strMessage.equalsIgnoreCase("option") || strMessage.equalsIgnoreCase("options")
					|| strMessage.equalsIgnoreCase("services") || strMessage.equalsIgnoreCase("Gaddi")
					|| strMessage.equalsIgnoreCase("RAIL") || strMessage.equalsIgnoreCase("RAILWAY")
					|| strMessage.equalsIgnoreCase("service") || strMessage.equalsIgnoreCase("IRCTC")) {
				Logs.getRequestLogs("Requesting for help:::::::::::::::" + strMessage);
				strOutPut = getHelpMessage();
			} else if (strMessage.equalsIgnoreCase("MHELP")) {
				strOutPut = getMHelpMessage();
			} else {
				String strErrorResponse = objMakeURL.checkMessage(strMessage);
				if (strErrorResponse.equals("OK")) {
					if (strMessage.startsWith("PLAN")) {
						strOutPut = "The ticket fare is 453.0  & the availability status is AVAILABLE- 0228 Your SMS Transaction ID is 501. Please send BOOKPAY [SMSTRANSACTIONID] [IMPS] [MMID] [OTP] to 139 to Pay booking amount";
					} else if (strMessage.startsWith("BOOKPAY")) {
						strOutPut = "Your ticket booked successfully with PNR number 2234330232.\nThanks for using SMS Booking.";
					} else {
						String strgetURL = objMakeURL.getUrl(strMessage, strMsisdn);
						Logs.getRequestLogs("Response from MakeUrl--> " + strId + "$" + strgetURL);
						// strOutPut =
						// objResponse.makeResponseUrl(objMakeURL.getUrl(strMessage,
						// strMsisdn));
						strOutPut = objResponse.makeResponseUrl(strgetURL);
						Logs.getRequestLogs("Response in process--> " + strId + "$" + strOutPut);
						this.keyword = objResponse.getKeyword();
					}
				} else {
					strOutPut = strErrorResponse;
				}
				if (strOutPut.indexOf(
						"Either PNR Number is not valid or data is not available The Data Is Unavailable") != -1) {
					strOutPut = "Sorry invalid PNR.Please enter valid 10 digit number";
				} else if (strOutPut.indexOf("REA ERROR :Please provide correct Station Code") != -1) {
					strOutPut = "Sorry invalid station code/name. Pls enter valid station name/code";
				} else if (strOutPut.indexOf("available from REA for FetchPNRInformation") != -1) {
					strOutPut = "Dear User, Information Requested by you is currently not available. Please try after sometime.";
				} else if (strOutPut.indexOf("Data is not available from REA for NTESTrainNTESInformation") != -1) {
					strOutPut = "Dear User, Information Requested by you is currently not available. Please try after sometime.";
				} else if (strOutPut
						.indexOf("Data is not available from REA for FetchTrainBetweenStationsInformations") != -1) {
					strOutPut = "Dear User, Information Requested by you is currently not available. Please try after sometime.";
				} else if (strOutPut.indexOf("REA ERROR : Please enter Valid Date") != -1
						&& keyword.equalsIgnoreCase("TimeTable Enquiry")) {
					strOutPut = "REA ERROR :Sorry Train is not running Today. Please Enter valid Train Number.";
				}
				Logs.getRequestLogs("In Process method----- " + strId + "," + strOutPut);
				objMakeURL = null;
				objResponse = null;

			}

		} catch (Exception e) {
			Logs.getErrorLogs("In process catch---- " + strId + "," + LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}
	}

	public synchronized String trim(String strString) {
		if (strString != null && !strString.equals("")) {
			return strString.trim();
		} else {
			return "";
		}
	}

	public String encoding(String encodingString) {
		encodingString = encodingString.replaceAll(" ", "%20");
		encodingString = encodingString.replaceAll(";", "");
		return encodingString;

	}

	public String getHelpMessage() {
		StringBuilder sb = new StringBuilder("Railway Enquiry.");
		sb.append(
				",1.PNR Status: PNR (PNR No),2.Train Arrival/Departure: AD (Tr No) (Stn STD Code),3.Train Current Position: SPOT (Tr No)");
		sb.append(
				",4.Ticket Booking Application: APP,5.Seat Avail: SEAT (Tr No) (Dt ddmmyy) (From Stn STD Code) (To Stn STD code) (Class) (Quota)");
		sb.append(",6.Tatkal Seat Avail: TSEAT (Tr No) (Dt ddmmyy) (To Stn Code) (To Stn Code) (Class)");

		this.keyword = "Help Message";
		return sb.toString();
	}

	public static boolean isCharacters(String s) {
		return s.matches("[a-zA-Z]+");
	}

	public String getMHelpMessage() {
		StringBuilder sb = new StringBuilder("Railway Enquiry.");
		sb.append(
				",1.TimeTable: TIME (Tr No),2.Train Name/Number: TN (Tr No/ Name),3.Next Train: NEXT (From Stn Code) (To Stn Code)");
		sb.append(",4.Train Route: ROUTE(Tr No),5.Station Code: CODE(Stn Name),6.Train Schedule: SCHEDULE(Tr No)");
		sb.append(",7.Fare Enquiry: FARE (Tr No) (Dt ddmmyy) (From Stn STD Code) (To Stn STD code) (Class) (Quota)")
				.append(",8.Trains " + "Between Two Stations: TRAIN (From Stn Code) (To Stn Code)");
		this.keyword = "Help Message";
		return sb.toString();
	}

	public String getMessage(String strMessage) {
		strMessage = getMessagePattern(strMessage,
				"\"|\\(|\\)|<|>|-|=|\\]|\\[|\\?|!|,|&|:|\\+|/|\\*|#|%|\\\\|\\}|\\{|~|'|\\^|\\r|\\n", "");
		strMessage = getMessagePattern(strMessage, "  |Dt|dt", " ");
		return strMessage;
	}

	public String getComment(String strOutputString) {
		MakeURL objM = new MakeURL();
		String numMessage = objM.getMessage(strMessage);
		strMessage = strMessage.toUpperCase();
		strOutputString = strOutputString.replaceAll("<br />", "#");
		strOutputString = strOutputString.replaceAll("<br /", "");
		if (strMessage.startsWith("T") || strMessage.startsWith("F") || strMessage.startsWith("AD")
				|| strMessage.startsWith("S") || strMessage.startsWith("PLAN") || strMessage.startsWith("BOOK")) {
		} else if (numMessage.length() == 10) {
			strOutputString = "PNR " + numMessage + "#" + strOutputString;
		}
		strOutputString = strOutputString.replaceAll("#Cur Stat:", "Curr Stat: ");
		if (strOutputString.indexOf("Name/Number") != -1) {
		} else {
			strOutputString = strOutputString.replaceAll("Number", "");
		}
		if (strMessage.startsWith("SPOT") || strMessage.startsWith("LOCATE")) {
			strOutputString = strOutputString.replaceAll("#", "\n");
			strOutputString = strOutputString.replaceAll("Stopping", "Stop");
			strOutputString = strOutputString.replaceAll("Arrival", "Arr");
			strOutputString = strOutputString.replaceAll("Schd", "Sch");
			Logs.getRequestLogs("In getComment----> " + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
					+ strMsisdn + "$" + strOutputString);
			return strOutputString;
		} else if (strMessage.startsWith("PLATFORM")) {
			strOutputString = strOutputString.replaceAll("#", "\n");
			if (strOutputString.indexOf("Stn To Start") != -1) {
				String strFirst = strOutputString.substring(0, strOutputString.indexOf("Stn To Start"));
				strOutputString = strOutputString.substring(strOutputString.indexOf("Stn To Start")).trim();
				strOutputString = strOutputString.substring(strOutputString.indexOf("Schd Platform")).trim();
				String strFinal = strFirst + strOutputString;
				strFinal = strFinal.replaceAll("\n\n", "\n");
				strFinal = strFinal.replaceAll("<br>", "\n");
				strFinal = strFinal.replaceAll("Stn", "Station");
				strFinal = strFinal.replaceAll("Schd", "Scheduled");
				strFinal = strFinal.replaceAll("Tr :", "Train:");
				Logs.getRequestLogs("In getComment----> " + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
						+ strMsisdn + "$" + strFinal);
				return strFinal;
			} else if (strOutputString.indexOf("Schd Arrival") != -1) {
				String strFirst = strOutputString.substring(0, strOutputString.indexOf("Schd Arrival"));
				strOutputString = strOutputString.substring(strOutputString.indexOf("Schd Arrival")).trim();
				strOutputString = strOutputString.substring(strOutputString.indexOf("Schd Platform")).trim();
				String strFinal = strFirst + strOutputString;
				strFinal = strFinal.replaceAll("\n\n", "\n");
				strFinal = strFinal.replaceAll("<br>", "\n");
				strFinal = strFinal.replaceAll("Stn", "Station");
				strFinal = strFinal.replaceAll("Schd", "Scheduled");
				strFinal = strFinal.replaceAll("Tr :", "Train:");
				Logs.getRequestLogs("In getComment----> " + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
						+ strMsisdn + "$" + strFinal);
				return strFinal;
			} else {
				Logs.getRequestLogs("In getComment----> " + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
						+ strMsisdn + "$" + strOutputString);
				return strOutputString;
			}

		} else {
			strOutputString = strOutputString.replaceAll("CL", "CLASS");
			strOutputString = strOutputString.replaceAll("#Train Name:", " - ");
			strOutputString = strOutputString.replaceAll("#Tr. Name:", " - ");
			strOutputString = strOutputString.replaceAll("#Booking", " Booking");
			strOutputString = strOutputString.replaceAll("#", ";,");
			strOutputString = strOutputString.replaceAll("595;,", "595#");
			strOutputString = strOutputString.replaceAll("113;,", "113#");
			strOutputString = strOutputString.replaceAll("104;,", "104#");
			// strOutputString = strOutputString.replaceAll(";,","\n");
			strOutputString = strOutputString.replaceAll("CHART", "");
			strOutputString = strOutputString.replaceAll("BoardingStation", "BoardingStn");
			strOutputString = strOutputString.replaceAll("ReservationUpTo", "ReservedUpTo");
			strOutputString = strOutputString.replaceAll("CLASST", "CLT");
			strOutputString = strOutputString.replaceAll("P1:Passenger-0", "P1:");
			strOutputString = strOutputString.replaceAll("P2:Passenger-1", "P2:");
			strOutputString = strOutputString.replaceAll("P3:Passenger-2", "P3:");
			strOutputString = strOutputString.replaceAll("P4:Passenger-3", "P4:");
			strOutputString = strOutputString.replaceAll("P5:Passenger-4", "P5:");
			strOutputString = strOutputString.replaceAll("P6:Passenger-5", "P6:");
			strOutputString = strOutputString.replaceAll("P7:Passenger-6", "P7:");
			strOutputString = strOutputString.replaceAll("P8:Passenger-7", "P8:");
			strOutputString = strOutputString.replaceAll("P9:Passenger-8", "P9:");
			strOutputString = strOutputString.replaceAll("P10:Passenger-9", "P10:");
			strOutputString = strOutputString.replaceAll("  ", " ");
			strOutputString = strOutputString.replaceAll("<br>", "");
			strOutputString = strOutputString.replaceAll("Date", "Dt");
			strOutputString = strOutputString.replaceAll(":", " :");
			strOutputString = strOutputString.replaceAll("\\|", ",");
			strOutputString = strOutputString.replaceAll("DaysOfRuning :Mon,Tue,Wed,Thi,Fri,Sat,Sun :",
					" Days of Run : ");
			strOutputString = strOutputString.replaceAll("DaysOfRuning :", " Days of Run :");
			// System.out.println("Message Befor Time table ===
			// \n"+strOutputString);
			if (strOutputString.indexOf("Days Of Running") != -1) {
				strOutputString = strOutputString.replaceAll(";,", ",");
				String strFinalOutput = "";
				int i = 0;
				while (strOutputString.indexOf("From Stn") != -1) {
					strFinalOutput = strFinalOutput + strOutputString.substring(strOutputString.indexOf("Tr. No"),
							strOutputString.indexOf("From Stn"));
					// System.out.println(strFinalOutput);
					strOutputString = strOutputString.substring(strOutputString.indexOf("From Stn") + 8).trim();
					// System.out.println(strOutputString);
					i = i + 1;
				}
				System.out.println("RESPONSE ===" + strFinalOutput);
				if (i > 10) // if No. of trains more than 10
				{
					// insert data in DB
					DataBaseOperation object = new DataBaseOperation();
					object.insertTrainBetweenTwoStnInfo(strId, strMsisdn, strMessage, strFinalOutput, i);
					// Truncate 10 trains information
					int j = 0;
					String strFirstMsg = "";
					while (strFinalOutput.indexOf("Tr. No") != -1) {
						if (strFinalOutput.length() > 2) {
							strFirstMsg = strFirstMsg + strFinalOutput.substring(0, strFinalOutput.indexOf(","));
							strFinalOutput = strFinalOutput.substring(strFinalOutput.indexOf(",") + 1).trim();
							strFirstMsg = strFirstMsg + ",";
							// System.out.println(strFirstMsg);
							j++;
							if (j == 10)
								break;
						} else {
							break;
						}
					}
					strFirstMsg = strFirstMsg + "For more trains send MORE to 139.";
					strFirstMsg = strFirstMsg.replaceAll(",", "\n");
					Logs.getRequestLogs("In getComment----> " + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
							+ strMsisdn + "$" + strFirstMsg);
					return strFirstMsg;
				} else {
					strFinalOutput = strFinalOutput.replaceAll(",", "\n");
					Logs.getRequestLogs("In getComment----> " + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
							+ strMsisdn + "$" + strFinalOutput);
					return strFinalOutput;
				}
			} else if (strOutputString.indexOf("ChartStatus") != -1) {
				String strFirst = strOutputString.substring(0, strOutputString.indexOf("BoardingStn"));
				strOutputString = strOutputString.substring(strOutputString.indexOf("BoardingStn")).trim();
				String strSecond = strOutputString.substring(0, strOutputString.indexOf("P1"));
				strOutputString = strOutputString.substring(strOutputString.indexOf("P1")).trim();
				String strThird = strOutputString.substring(0, strOutputString.indexOf("Dt"));
				String strNewThird = "";
				while (strThird.indexOf("Booking") != -1) {
					strNewThird = strNewThird + strThird.substring(0, strThird.indexOf("Booking"));
					strThird = strThird.substring(strThird.indexOf(";,") + 2).trim();
				}
				strNewThird = strThird + strNewThird;
				strNewThird = strNewThird.replaceAll("P2", ";;,P2");
				strNewThird = strNewThird.replaceAll("P3", ";;,P3");
				strNewThird = strNewThird.replaceAll("P4", ";;,P4");
				strNewThird = strNewThird.replaceAll("P5", ";;,P5");
				strNewThird = strNewThird.replaceAll("P6", ";;,P6");
				strNewThird = strNewThird.replaceAll("CLASS", ";;,CLASS");
				strOutputString = strOutputString.substring(strOutputString.indexOf("Dt")).trim();
				String strFourth = strOutputString.substring(0, strOutputString.indexOf("ChartStatus"));
				strOutputString = strOutputString.substring(strOutputString.indexOf("ChartStatus")).trim();
				strSecond = strSecond.replaceAll(";,", "\n");
				strFirst = strFirst.replaceAll(";,", "\n");
				strFourth = strFourth.replaceAll(";,", "\n");
				strNewThird = strNewThird.replaceAll(";,", "\n");
				strOutputString = strOutputString.replaceAll(";,", "\n");
				String strFinalmsg = strFirst + strFourth + strSecond + strNewThird + strOutputString;
				strFinalmsg = strFinalmsg.replaceAll("PREPARED,", "PREPARED");
				strFinalmsg = strFinalmsg.replaceAll("ChartStatus", "\nChartStatus");
				strFinalmsg = strFinalmsg.replaceAll("\n;\n", ";\n");
				// System.out.println("String is === "+strFinalmsg);
				strFinalmsg = strFinalmsg.replaceAll("  ", " ");
				strFinalmsg = strFinalmsg.replaceAll(" :", ":");
				strFinalmsg = strFinalmsg.replaceAll(": ", ":");
				strFinalmsg = strFinalmsg.replaceAll(":", " : ");
				strFinalmsg = strFinalmsg.replaceAll(" ;", ";");
				Logs.getRequestLogs("In getComment----> " + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
						+ strMsisdn + "$" + strFinalmsg);
				return strFinalmsg;
			} else if (strOutputString.indexOf("Days of Run :") != -1) {
				strOutputString = strOutputString.replaceAll(";,Tr. No", "Tr. No");
				strOutputString = strOutputString.replaceAll(";,;,", ";,");
				// System.out.println("Time Table Enquiry
				// Message.=====\n\n"+strOutputString);
				String strFirst = strOutputString.substring(0, strOutputString.indexOf("Days of Run"));
				strOutputString = strOutputString.substring(strOutputString.indexOf("Days of Run")).trim();
				String strSecond = strOutputString.substring(0, strOutputString.indexOf(";,"));
				strSecond = strSecond.substring(strSecond.indexOf(":")).trim();
				strOutputString = strOutputString.substring(strOutputString.indexOf(";,")).trim();
				String strNewSecond = "";
				String strDay = strSecond.substring(0, strSecond.indexOf(","));
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				if (strDay.startsWith(": Y")) {
					strNewSecond = "Mon";
				}
				strSecond = strSecond.substring(1);
				strDay = strSecond.substring(0, strSecond.indexOf(","));
				// System.out.println("after Sunday day"+strDay);
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				if (strDay.startsWith("Y")) {
					strNewSecond = strNewSecond + ",Tue";
				}
				strSecond = strSecond.substring(1);
				strDay = strSecond.substring(0, strSecond.indexOf(","));
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				if (strDay.startsWith("Y")) {
					strNewSecond = strNewSecond + ",Wed";
				}
				strSecond = strSecond.substring(1);
				strDay = strSecond.substring(0, strSecond.indexOf(","));
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				if (strDay.startsWith("Y")) {
					strNewSecond = strNewSecond + ",Thu";
				}
				strSecond = strSecond.substring(1);
				strDay = strSecond.substring(0, strSecond.indexOf(","));
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				if (strDay.startsWith("Y")) {
					strNewSecond = strNewSecond + ",Fri";
				}
				strSecond = strSecond.substring(1);
				strDay = strSecond.substring(0, strSecond.indexOf(","));
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				if (strDay.startsWith("Y")) {
					strNewSecond = strNewSecond + ",Sat";
				}
				if (strSecond.startsWith(",Y")) {
					strNewSecond = strNewSecond + ",Sun";
				}
				// strFirst=strFirst.replaceAll(",","<br>");
				// strNewSecond=strNewSecond.replaceAll(",",",;");
				String FinalMessage = strFirst + "Days of Run : " + strNewSecond + strSecond.substring(2)
						+ strOutputString;
				FinalMessage = FinalMessage.replaceAll(" Days of Run : ,", "Days of Run : ");
				FinalMessage = FinalMessage.replaceAll("DeptTime", "DeptTime ");
				FinalMessage = FinalMessage.replaceAll("ArrivalTime", "ArrivalTime ");
				FinalMessage = FinalMessage.replaceAll("Mon,Tue,Wed,Thu,Fri,Sat,Sun", "Daily");
				FinalMessage = FinalMessage.replaceAll("Tr. No", "TRAIN");
				FinalMessage = FinalMessage.replaceAll(";,DOJ", " Hrs ;,DOJ");
				String FirstMessage = FinalMessage.substring(0, FinalMessage.indexOf("DeptTime"));
				FinalMessage = FinalMessage.substring(FinalMessage.indexOf("DeptTime")).trim();
				String SecondMessage = FinalMessage.substring(0, FinalMessage.indexOf("To Stn"));
				SecondMessage = SecondMessage.replaceAll(",", " Hrs");
				SecondMessage = SecondMessage.replaceAll("; Hrs", ";,");
				FinalMessage = FinalMessage.substring(FinalMessage.indexOf("To Stn")).trim();
				FinalMessage = FirstMessage + SecondMessage + FinalMessage;

				// System.out.println("TIME TABLE FINAL MESSAGE is ======= "+
				// FinalMessage);

				FinalMessage = FinalMessage.replaceAll(";,", "\n");
				Logs.getRequestLogs("In getComment----> " + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
						+ strMsisdn + "$" + FinalMessage);
				return FinalMessage;
			} else if (strOutputString.indexOf("Late by") != -1) {
				strOutputString = strOutputString.replaceAll("Tr  :", "TRAIN :");
				String strFirst = "";
				if (strOutputString.indexOf("Stn To Start") != -1) {
					strFirst = strOutputString.substring(0, strOutputString.indexOf("Stn To Start"));
					strOutputString = strOutputString.substring(strOutputString.indexOf("Stn To Start"));
				} else {
					strFirst = strOutputString.substring(0, strOutputString.indexOf("Schd Arrival"));
					strOutputString = strOutputString.substring(strOutputString.indexOf("Schd Arrival")).trim();
				}
				String strSecond = strOutputString.substring(0, strOutputString.indexOf(";,Late by"));
				strOutputString = strOutputString.substring(strOutputString.indexOf(";,Late by")).trim();
				strSecond = strSecond.replaceAll(";,", ";");
				strSecond = strSecond.replaceAll(";Exp Arrival", " Hrs;,Exp Arrival");
				strSecond = strSecond.replaceAll(";Exp Dept", " Hrs;,Exp Dept");
				String FinalMsg = strFirst + strSecond + strOutputString;
				FinalMsg = FinalMsg.replaceAll(";,Late by", " Hrs;,Late by");
				FinalMsg = FinalMsg.replaceAll(";,", "\n");
				Logs.getRequestLogs("In getComment----> " + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
						+ strMsisdn + "$" + FinalMsg);
				return FinalMsg;
			} else {
				// strOutputString = strOutputString.replaceAll(","," ");
				strOutputString = strOutputString.replaceAll("Tr. No", "TRAIN");
				strOutputString = strOutputString.replaceAll("Has arrived", "Arrived");
				strOutputString = strOutputString.replaceAll(";,", "\n");
				Logs.getRequestLogs("In getComment----> " + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
						+ strMsisdn + "$" + strOutputString);
				return strOutputString;
			}
		}
	}

	public String getMessagePattern(String strMessage, String pattern, String replaceWith) {

		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(strMessage);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			m.appendReplacement(sb, replaceWith);
		}
		m.appendTail(sb);
		return sb.toString();

	}
	
}
