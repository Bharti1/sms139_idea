package com.spice.sms139.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbutils.DbUtils;

import com.spice.sms139.railways.db.DBManager;
import com.spice.sms139.utilities.LogStackTrace;
import com.spice.sms139.utilities.Logs;

public class RequestProcessor {

	static int intCounterThread = 0;

	@SuppressWarnings("resource")
	public RequestProcessor() {

		Connection objConnection = null;
		PreparedStatement objStatement = null, updateStatement = null, insertStatement = null;

		ResultSet objResulSet = null;
		String strQuery, insertQuery, updateQuery;
		try {

			strQuery = "SELECT id, Msisdn, SourceAdderss, Message, UniqueId, DateTime, Keyword, Udh, Dcs, Vmn, Imsi, CircleId,userName,password,retry_count FROM tbl_http_request where (status=? or (status = ? and retry_time <= NOW() and retry_count < 4)) and UniqueId =? order by id desc  limit 0,20";
			insertQuery = "INSERT INTO tbl_http_request_log(date_time,service_type,message_id,identifier,mobile,source,message,keyword) VALUES (STR_TO_DATE(?,'%Y-%m-%d %H:%i:%s'),?,?,?,?,?,?,?)";
			updateQuery = "update tbl_http_request  set status=? where id=?";

			while (true) {
				objConnection = DBManager.getConnection();
				if (objStatement == null || objStatement.isClosed()) {
					objStatement = objConnection.prepareStatement(strQuery);
				}
				if (updateStatement == null || updateStatement.isClosed()) {
					updateStatement = objConnection.prepareStatement(updateQuery);
				}
				if (insertStatement == null || insertStatement.isClosed()) {
					insertStatement = objConnection.prepareStatement(insertQuery);
				}
				objStatement.setString(1, "0");
				objStatement.setString(2, "2");
				objStatement.setString(3, "ida");
				try {
					objResulSet = objStatement.executeQuery();
					while (objResulSet.next()) {
						String strId = objResulSet.getString(1);
						String strMsisdn = objResulSet.getString(2);
						String strSourceAdderss = objResulSet.getString(3);
						String strMessage = objResulSet.getString(4);
						String strUniqueId = objResulSet.getString(5);
						String strDateTime = objResulSet.getString(6);
						String strKeyword = objResulSet.getString(7);
						String strUdh = objResulSet.getString(8);
						String strDcs = objResulSet.getString(9);
						String strVmn = objResulSet.getString(10);
						String strImsi = objResulSet.getString(11);
						String strCircleId = objResulSet.getString(12);
						String strUserName = objResulSet.getString(13);
						String strPassword = objResulSet.getString(14);
						int strRetryCount = objResulSet.getInt(15);
						if (checkThreadCounter()) {
							try {
								updateStatement.setString(1, "1");
								updateStatement.setString(2, strId);
								updateStatement.executeUpdate();

								insertStatement.setString(1, strDateTime);
								insertStatement.setString(2, "HTTP");
								insertStatement.setString(3, strId);
								insertStatement.setString(4, strUniqueId);
								insertStatement.setString(5, strMsisdn);
								insertStatement.setString(6, "139");
								insertStatement.setString(7, strMessage);
								insertStatement.setString(8, "NA");
								insertStatement.executeUpdate();
							} catch (Exception e) {
								System.out.println("Exception of duplicate entry for " + strId);
								Logs.getErrorLogs("Exception of duplicate entry for " + e.getMessage());
							}
							Logs.getRequestLogs(strId + "$" + strMsisdn + "$" + strSourceAdderss + "$" + strMessage
									+ "$" + strUniqueId + "$" + strDateTime + "$" + strKeyword + "$" + strUdh + "$"
									+ strDcs + "$" + strVmn + "$" + strImsi + "$" + strCircleId + "$" + strUserName
									+ "$" + strPassword);

							new MiddleWare(strId, strMsisdn, strSourceAdderss, strMessage, strUniqueId, strDateTime,
									strKeyword, strUdh, strDcs, strVmn, strImsi, strCircleId, strUserName, strPassword,
									strRetryCount);
						}
						objStatement.clearParameters();
						updateStatement.clearParameters();
						insertStatement.clearParameters();
					}

					DbUtils.closeQuietly(objStatement);
					DbUtils.closeQuietly(updateStatement);
					DbUtils.closeQuietly(insertStatement);

					Thread.sleep(1000);
				} catch (Exception objException) {
					Logs.getErrorLogs("In RequestProcessor of IDEA " + LogStackTrace.getStackTrace(objException));
					objException.printStackTrace();
				}

			}
		} catch (Exception e) {
			Logs.getErrorLogs("In RequestProcessor of IDEA-2 " + LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			DbUtils.closeQuietly(updateStatement);
			DbUtils.closeQuietly(insertStatement);
			DbUtils.closeQuietly(objResulSet);
			DbUtils.closeQuietly(objConnection);
		}

	}

	public boolean checkThreadCounter() {
		try {
			while (true) {
				if (intCounterThread < 20) {
					intCounterThread++;
					System.out.println("----------------intCounterThread -----------" + intCounterThread);
					break;
				} else {
					Thread.sleep(1000);
				}

			}
		} catch (Exception objException) {
			objException.printStackTrace();
		}
		return true;
	}
}
