package com.spice.sms139.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.commons.dbutils.DbUtils;

import com.spice.sms139.railways.db.DBManager;
import com.spice.sms139.utilities.LogStackTrace;
import com.spice.sms139.utilities.Logs;

public class GetNextTrainInfo {

	public String getInformation(String strMsisdn, String strMessage) {
		strMessage = strMessage.trim().toUpperCase();

		String strMessageID = "", strMobile = "", strRespMsg = "", strFinalMsg = "";
		int intTotalTrains = 0, intSmsSent = 0;
		int intAct = 0;
		Connection objConnection = null;
		Statement objStatement = null;
		ResultSet rs = null;
		// check for active request
		try {
			String strQuery = "select count(1) from tbl_train_two_station where mobile = '" + strMsisdn
					+ "' and status ='A' order by date_time limit 1";
			objConnection = DBManager.getConnection();
			objStatement = objConnection.createStatement();
			rs = objStatement.executeQuery(strQuery);
			while (rs.next()) {
				intAct = rs.getInt(1);
			}
		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(objStatement);
			DbUtils.closeQuietly(objConnection);
		}
		if (intAct >= 1)// check for active request
		{
			// Get request details from DB.
			try {
				String strQuery = "select message_id,mobile,resp_message,total_trains,sms_sent from tbl_train_two_station where mobile = '"
						+ strMsisdn + "' and status ='A' order by date_time limit 1";
				objConnection = DBManager.getConnection();
				objStatement = objConnection.createStatement();
				rs = objStatement.executeQuery(strQuery);
				while (rs.next()) {
					strMessageID = rs.getString(1);
					strMobile = rs.getString(2);
					strRespMsg = rs.getString(3);
					intTotalTrains = rs.getInt(4);
					intSmsSent = rs.getInt(5);
				}
				System.out.println("mobile number is:- " + strMobile);
				int intNextSMS = intSmsSent * 10;
				int intTinfolen = (intSmsSent + 1) * 10;
				// Break Response message for next trains.
				int j = 0;
				strRespMsg = strRespMsg.substring(0, strRespMsg.length() - 1);
				if (intTotalTrains == intNextSMS + 1) {
					strFinalMsg = strRespMsg.substring(strRespMsg.lastIndexOf(","), strRespMsg.length());
					updateStatus(strMessageID, intSmsSent + 1, "D");
				} else {
					while (strRespMsg.indexOf("Tr. No") != -1) {
						if (strRespMsg.length() > 2) {
							strRespMsg = strRespMsg.substring(strRespMsg.indexOf(",") + 1).trim();
							j = j + 1;
							System.out.println("Inside ResponseMessage ===" + strRespMsg);
							if (j >= intNextSMS) {
								strFinalMsg = strFinalMsg + strRespMsg.substring(0, strRespMsg.indexOf(","));
								strFinalMsg = strFinalMsg + ",";
								System.out.println("strFinalMsg===" + strFinalMsg);
							}
							if (j == intTotalTrains - 2) {
								String strlastTrain = strRespMsg.substring(strRespMsg.lastIndexOf(",") + 1,
										strRespMsg.length());
								strFinalMsg = strFinalMsg + strlastTrain;
								updateStatus(strMessageID, intSmsSent + 1, "D");
								break;
							}
							if (j == intTinfolen - 1) {
								strFinalMsg = strFinalMsg + "For more trains send More to 139.";
								updateStatus(strMessageID, intSmsSent + 1, "A");
								break;
							}

						} else {
							// Deactivate status from table
							updateStatus(strMessageID, intSmsSent + 1, "D");
							break;
						}

					}
				}

				System.out.println("Next Response Msg ==== " + strFinalMsg);
			} catch (Exception ex) {
				Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
				ex.printStackTrace();
			} finally {
				DbUtils.closeQuietly(rs);
				DbUtils.closeQuietly(objStatement);
				DbUtils.closeQuietly(objConnection);
			}
		} else // No active request send Help message.
		{
			strFinalMsg = "To know trains between two stations. SMS Train [source station code] [destination stations code] to 139. Example : TRAIN NDLS CDG";
		}
		// return
		strFinalMsg = strFinalMsg.replaceAll(",", "<br>");
		return strFinalMsg;
	}

	public void updateStatus(String strMessageID, int intSMSSent, String strStatus) {
		Connection objConnection = null;
		Statement objStatement = null;
		try {
			String strQuery = "update tbl_train_two_station set status ='" + strStatus + "',sms_sent='" + intSMSSent
					+ "' where message_id='" + strMessageID + "'";
			objConnection = DBManager.getConnection();
			objStatement = objConnection.createStatement();
			objStatement.executeUpdate(strQuery);
		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			DbUtils.closeQuietly(objConnection);
		}

	}
}
