package com.spice.sms139.service;

import java.util.Calendar;

import com.spice.sms139.commons.LoadConfiguration;
import com.spice.sms139.commons.LoadInfo;
import com.spice.sms139.railways.db.DataBaseOperation;
import com.spice.sms139.utils.OsType;

public class MakeURL {

	int intcheckSpace = 0;

	public synchronized String getUrl(String strMessage, String strMsisdn) throws Exception {
		LoadConfiguration objLoadConfiguration = new LoadConfiguration();
		String location = "/home/sms139_services/";
		if (OsType.isWindows()) {
			location = "E:/sms139_services/";
		}
		String fileName = "railway.cfg";
		objLoadConfiguration.loadPrimaryFile(location + fileName);
		objLoadConfiguration.loadProperties(location + LoadInfo.strPrimaryFile);

		strMessage = strMessage.toUpperCase();
		if (strMessage.startsWith("TRAIN") || strMessage.startsWith("SEAT")) {
			if (strMessage.matches(".*\\d.*")) {
				if (intcheckSpace == 3) {
					return getTrainEnquireUrl(strMessage);
				} else {
					if (intcheckSpace == 5) {
						strMessage = strMessage + " G";
					} // By Default ClassType Set , added by MohitM
					return getTrainAccomadationUrl(strMessage);
				}
			} else {
				return getTrainTwoStn(strMessage);
			}

		} else if (strMessage.startsWith("F") || strMessage.startsWith("f")) {
			if (intcheckSpace == 5) {
				strMessage = strMessage + " G";
			} // By Default ClassType Set , added by MohitM
			return getFareUrl(strMessage);
		} else if (strMessage.startsWith("AD") || strMessage.startsWith("PLATFORM")) {
			return getTrainEnquireUrl(strMessage);
		} else if (strMessage.startsWith("TN")) {
			return getTrainDetailUrl(strMessage);
		} else if (strMessage.startsWith("TIME") || strMessage.startsWith("TT")) {
			return getTrainTimetableUrl(strMessage);
		} else if (strMessage.startsWith("LOCATE") || strMessage.startsWith("SPOT")) {
			return getCurrentTrainInfo(strMessage);
		} else if (strMessage.startsWith("WET")) {
			return getWetherUrl(strMessage);
		} else if (strMessage.startsWith("SPM")) {
			return getSpiceAppURL(strMessage, strMsisdn);
		} else if (strMessage.startsWith("C ")) {
			return getChennaiURL(strMessage, strMsisdn);
		} else if (strMessage.startsWith("PLAN")) {
			return planValidation(strMessage);
		} else if (strMessage.startsWith("BOOKPAY")) {
			return bookPayValidation(strMessage);
		} else {
			return getPnrEnquireUrl(strMessage);
		}
	}

	public synchronized String getPnrEnquireUrl(String strMessage) throws Exception {
		strMessage = getMessage(strMessage);
		return "http://" + LoadInfo.strPrimaryIP + "/Pnrenquirynew/Default.aspx?pnr=" + strMessage;
	}

	public synchronized String getWetherUrl(String strMessage) throws Exception {
		String strCityName = "";
		strCityName = strMessage.substring(strMessage.indexOf(" ")).trim();
		return "http://wap.weather.fi/peek?param1=" + strCityName + "&wind=mpsu1";
	}

	public synchronized String getChennaiURL(String strMessage, String strMsisdn) throws Exception {
		String strCityName = "";
		strCityName = strMessage.substring(strMessage.indexOf(" ")).trim();
		return "http://203.199.176.8/ATTSSMS/ProcessSMS.aspx?sender=" + strMsisdn + "&msg=" + strCityName;
	}

	public synchronized String getCurrentTrainInfo(String strMessage) throws Exception {
		String strTrainNumber = "";
		strTrainNumber = strMessage.substring(strMessage.indexOf(" ")).trim();
		return "http://" + LoadInfo.strPrimaryIP + "/trainLocatioN/Location.aspx?trainNo=" + strTrainNumber;
	}

	public synchronized String getSpiceAppURL(String strMessage, String strMsisdn) throws Exception {
		strMessage = strMessage.replaceAll(" ", "%20");
		return "http://mobile.cellebrum.com/salestracking/getsales.api?mode=S&msisdn=" + strMsisdn + "&strval="
				+ strMessage + "";
	}

	public synchronized String getTrainDetailUrl(String strMessage) throws Exception {
		String strCityName = "";
		strCityName = strMessage.substring(strMessage.indexOf(" ")).trim();
		strCityName = strCityName.replaceAll(" ", "%20");
		return "http://" + LoadInfo.strPrimaryIP + "/tinfo/trainrequest.aspx?tn=" + strCityName;
	}

	public synchronized String getTrainTwoStn(String strMessage) throws Exception {
		DataBaseOperation db = new DataBaseOperation();
		String strSecondWord = "", strDestStn = "", strDestStnCode = "", strSourceStn = "", strSourceStnCode = "";
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		if (intcheckSpace == 2) // Source and dest station in one word only.
		{
			strSourceStn = strMessage.substring(0, strMessage.indexOf(" "));
			strDestStn = strMessage.substring(strMessage.indexOf(" ")).trim();
			// check for Source stnCode
			strSourceStnCode = db.checkStnCode(strSourceStn);
			if (strSourceStnCode.equalsIgnoreCase("OTHER")) {
				strSourceStnCode = db.getStnCode(strSourceStn);
			}
			// Check for Dest stnCode
			strDestStnCode = db.checkStnCode(strDestStn);
			if (strDestStnCode.equalsIgnoreCase("OTHER")) {
				strDestStnCode = db.getStnCode(strDestStn);
			}
		} else {
			strSourceStn = strMessage.substring(0, strMessage.indexOf(" "));
			strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
			// Check for first station code
			strSourceStnCode = db.getStnCode(strSourceStn);
			if (strSourceStnCode.equalsIgnoreCase("OTHER")) {
				strSecondWord = strMessage.substring(0, strMessage.indexOf(" "));
				strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
				strSourceStn = strSourceStn + " " + strSecondWord;
				strSourceStnCode = db.getStnCode(strSourceStn);
				strDestStn = strMessage;
				strDestStnCode = db.getStnCode(strDestStn);
			} else {
				strDestStn = strMessage;
				strDestStnCode = db.getStnCode(strDestStn);

			}
		}
		System.out.println("Source Station = " + strSourceStn);
		System.out.println("Source Stn Code = " + strSourceStnCode);
		System.out.println("Destionation Station =" + strDestStn);
		System.out.println("Dest Stn Code" + strDestStnCode);
		String strDateOfJourney = getDate();
		return "http://" + LoadInfo.strPrimaryIP + "/TbsWithTime/TBS.aspx?From=" + strSourceStnCode + "&To="
				+ strDestStnCode + "&doj=" + strDateOfJourney;
	}

	public synchronized String getTrainTimetableUrl(String strMessage) throws Exception {
		String strTrainNumber = "";
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		if (intcheckSpace == 1) {
			strTrainNumber = strMessage;
		} else {
			strTrainNumber = strMessage.substring(strMessage.indexOf(" ")).trim();
		}
		return "http://" + LoadInfo.strPrimaryIP + "/TimeTable/timetable.aspx?trnno=" + strTrainNumber;
	}

	public synchronized String getTrainEnquireUrl(String strMessage) throws Exception {
		if (strMessage.startsWith("PNR")) {
			return getPnrEnquireUrl(strMessage);
		}
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strTrainNumber = strMessage.substring(0, strMessage.indexOf(" "));
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strDateOfJourney = "", strTrainStatingStationStdCode = "";
		if (intcheckSpace == 3) {
			strDateOfJourney = strMessage.substring(0, strMessage.indexOf(" "));
			String str1dayafter = getDate_1dayafter();
			String str2dayafter = getDate_2dayafter();
			if (!strDateOfJourney.equalsIgnoreCase(str1dayafter) || !strDateOfJourney.equalsIgnoreCase(str2dayafter)) {
				strDateOfJourney = getDate();
			}
			strTrainStatingStationStdCode = strMessage.substring(strMessage.indexOf(" ")).trim();
		} else {
			strDateOfJourney = getDate();
			strTrainStatingStationStdCode = strMessage;
		}
		return "http://" + LoadInfo.strPrimaryIP + "/trainrequest/trainrequest.aspx?trnno=" + strTrainNumber + "&doj="
				+ strDateOfJourney + "&stdcode=" + strTrainStatingStationStdCode;
	}

	public synchronized String getTrainAccomadationUrl(String strMessage) throws Exception {
		if (strMessage.startsWith("PNR") || strMessage.startsWith("p") || strMessage.startsWith("P")) {
			return getPnrEnquireUrl(strMessage);
		}
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strTrainNumber = strMessage.substring(0, strMessage.indexOf(" "));
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strDateOfJourney = strMessage.substring(0, strMessage.indexOf(" "));
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strTrainStatingStationStdCode = strMessage.substring(0, strMessage.indexOf(" ")).trim();
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strTrainEndingStationStdCode = strMessage.substring(0, strMessage.indexOf(" ")).trim();
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strClass = strMessage.substring(0, strMessage.indexOf(" ")).trim();
		strClass = strClass.substring(0, 2);
		String strClassType = strMessage.substring(strMessage.indexOf(" ")).trim();
		return "http://" + LoadInfo.strPrimaryIP + "/accomodation/accomodation.aspx?trnno=" + strTrainNumber + "&doj="
				+ strDateOfJourney + "&origine=" + strTrainStatingStationStdCode + "&destination="
				+ strTrainEndingStationStdCode + "&class=" + strClass + "&type=" + strClassType;
	}

	public synchronized String getFareUrl(String strMessage) throws Exception {
		if (strMessage.startsWith("p") || strMessage.startsWith("P")) {
			return getPnrEnquireUrl(strMessage);
		}
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strTrainNumber = strMessage.substring(0, strMessage.indexOf(" "));
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strDateOfJourney = strMessage.substring(0, strMessage.indexOf(" "));
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strTrainStatingStationStdCode = strMessage.substring(0, strMessage.indexOf(" ")).trim();
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strTrainEndingStationStdCode = strMessage.substring(0, strMessage.indexOf(" ")).trim();
		strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
		String strClass = strMessage.substring(0, strMessage.indexOf(" ")).trim();
		strClass = strClass.substring(0, 2);
		String strClassType = strMessage.substring(strMessage.indexOf(" ")).trim();
		return "http://" + LoadInfo.strPrimaryIP + "/fare/fare.aspx?trnno=" + strTrainNumber + "&doj="
				+ strDateOfJourney + "&origine=" + strTrainStatingStationStdCode + "&destination="
				+ strTrainEndingStationStdCode + "&class=" + strClass + "&type=" + strClassType;
	}

	public synchronized String checkMessage(String strMessage) throws Exception {
		intcheckSpace = findValidSpace(strMessage);
		strMessage = strMessage.toUpperCase();
		if (strMessage.startsWith("TRAIN") || strMessage.startsWith("SEAT")) {
			return "OK";
			// return trainAccValidation(strMessage);
		} else if (strMessage.startsWith("AD")) {
			return trainEnquireValidation(strMessage);
		} else if (strMessage.startsWith("PLATFORM")) {
			return platformEnquireValidation(strMessage);
		} else if (strMessage.startsWith("F") || strMessage.startsWith("f")) {
			return trainFareValidation(strMessage);
		} else if (strMessage.startsWith("TIME") || strMessage.startsWith("TT")) {
			return trainTimetableValidation(strMessage);
		} else if (strMessage.startsWith("LOCATE") || strMessage.startsWith("SPOT")) {
			return locateValidation(strMessage);
		} else if (strMessage.startsWith("WET")) {
			return whetherValidation(strMessage);
		} else if (strMessage.startsWith("SPM")) {
			return "OK";
		} else if (strMessage.startsWith("TN")) {
			return "OK";
		} else if (strMessage.startsWith("C ")) {
			return "OK";
		} else if (strMessage.startsWith("PLAN")) {
			return planValidation(strMessage);
		} else if (strMessage.startsWith("BOOKPAY")) {
			return bookPayValidation(strMessage);
		} else {
			return pnrValidation(strMessage);
		}
	}

	public String locateValidation(String strMessage) {
		if (intcheckSpace == 1) {
			return "OK";
		} else {
			return "Dear User you have entered wrong syntax.,To know current position of your train, SMS SPOT <train number> to 139";
		}
	}

	public String trainTimetableValidation(String strMessage) {
		if (intcheckSpace == 1 || intcheckSpace == 2) {
			return "OK";
		} else {
			return "Thanks for using 139 Rail Enquiry Service. You have sent an invalid information. For TRAIN TIME TABLE Enquiry,please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
		}
	}

	public String planValidation(String strMessage) {
		if (intcheckSpace == 8) {
			return "OK";
		} else {
			return "Dear User you have entered wrong syntax.\nTo book ticket Please send PLAN [userid] [Password] [Train No] [source stn code] [destinaton stn code] [Date of Journey ddmmyy] [class] [Registerd Passenger List Name] to 139 \nExample : PLAN sandeepkr sandi123 12012 CDG NDLS 151012 CC LIST1";
		}
	}

	public String bookPayValidation(String strMessage) {
		if (intcheckSpace == 4) {
			return "OK";
		} else {
			return "Dear User you have entered wrong syntax.\nTo Pay ticket booking, Please send BOOKPAY [SMSTRANSACTIONID] [IMPS] [MMID] [OTP] to 139 \nExample : BOOKPAY 501 IMPS 101293 938394";
		}
	}

	public String trainAccValidation(String strMessage) {
		if ((intcheckSpace == 6) || (intcheckSpace == 5) || (intcheckSpace == 3)) {
			strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
			String strTrainNumber = strMessage.substring(0, strMessage.indexOf(" "));
			if (strTrainNumber.length() < 5) {
				return "Dear user, train numbers have changed from 4 to 5 digit please enter the correct 5 digit Tr. no. To know your 5 digit train number SMS TN <Train Name> to 139 Example: TN K V EXP";
			} else {
				return "OK";
			}
		} else {
			return "Thanks for using 139 Railway Enquiry Service. You have sent an invalid information. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
		}
	}

	public String trainEnquireValidation(String strMessage) {
		if (intcheckSpace == 2) {
			return "OK";
		} else {
			return "Thanks for using 139 Rail Enquiry Service. You have sent an invalid information. For Train Arrival Departure enquiry, please CALL 139 or SMS AD <Train No.> <STD code> to 139. For e.g.: AD 12012 011. Sorry for the inconvenience.";
		}
	}

	public String platformEnquireValidation(String strMessage) {
		if (intcheckSpace == 2) {
			return "OK";
		} else {
			return "Thanks for using 139 Rail Enquiry Service. You have sent an invalid information. For Train Platform Enquiry, SMS PLATFORM <Train No.> <STD code> to 139. For e.g.: PLATFORM 12012 011. Sorry for the inconvenience.";
		}
	}

	public String whetherValidation(String strMessage) {
		if (intcheckSpace < 4) {
			return "OK";
		} else {
			return "Dear user you have entered wrong syntax. For Weather enquiry type WET <City name> and send to 139. Example WET Chandigarh";
		}

	}

	public String trainFareValidation(String strMessage) {
		if (intcheckSpace == 6 || intcheckSpace == 5) {
			return "OK";
		} else {
			return "Thanks for using 139 Railway Enquiry Service. You have sent an invalid information. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
		}
	}

	public String pnrValidation(String strMessage) {
		strMessage = getMessage(strMessage);
		if (strMessage == null || strMessage.equalsIgnoreCase("") || strMessage.equalsIgnoreCase(" ")
				|| strMessage.equalsIgnoreCase("null") || strMessage.length() < 5) {
			return "Railway Enquiry.,1.PNR Status :PNR (PNR No),2.Train Arrival/Departure :AD (Tr No) (Stn STD Code),3.Train Current Position :SPOT (Tr No),4.Next Train :NEXT (From Stn Code) (To Stn Code),5.Seat Avail :SEAT (Tr No) (Dt ddmmyy) (From Stn STD Code) (To Stn STD code) (Class) (Quota),6.Tatkal Seat Avail :TSEAT (Tr No) (Dt ddmmyy) (To Stn Code) (To Stn Code) (Class),7.Platform Info :PLATFORM (Tr No)(Stn STD Code).For more help on Railway Enquiry, SMS MHELP to 139";
		}

		else if (strMessage != null && strMessage.length() < 10) {
			return "Dear User, PNR number should be of 10 digits. You have sent an invalid information. For PNR status enquiry  please CALL 139 or SMS PNR <10 digit PNR number> to 139. For e.g.  : PNR 2345678901. We regret the inconvenience caused.";
		} else {
			return "OK";
		}
	}

	public synchronized int findValidSpace(String strMessage) throws Exception {
		int intCountspace = 0;
		while (strMessage.indexOf(" ") != -1) {
			strMessage = strMessage.substring(strMessage.indexOf(" ")).trim();
			intCountspace++;
		}
		return intCountspace;

	}

	public static String getDate() {
		Calendar calendar = Calendar.getInstance();
		String strYear = Integer.toString(calendar.get(1));
		int month = calendar.get(2) + 1;
		int day = calendar.get(5);
		String strMonth = Integer.toString(month);
		String strDay = Integer.toString(day);
		if (month < 10)
			strMonth = "0" + strMonth;
		if (day < 10)
			strDay = "0" + strDay;
		String str = strDay + "" + strMonth + "" + strYear.substring(2);
		return str;
	}

	public static String getDate_1dayafter() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		String strYear = Integer.toString(calendar.get(1));
		int month = calendar.get(2) + 1;
		int day = calendar.get(5);
		String strMonth = Integer.toString(month);
		String strDay = Integer.toString(day);
		if (month < 10)
			strMonth = "0" + strMonth;
		if (day < 10)
			strDay = "0" + strDay;
		String str = strDay + "" + strMonth + "" + strYear.substring(2);
		return str;
	}

	public static String getDate_2dayafter() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 2);
		String strYear = Integer.toString(calendar.get(1));
		int month = calendar.get(2) + 1;
		int day = calendar.get(5);
		String strMonth = Integer.toString(month);
		String strDay = Integer.toString(day);
		if (month < 10)
			strMonth = "0" + strMonth;
		if (day < 10)
			strDay = "0" + strDay;
		String str = strDay + "" + strMonth + "" + strYear.substring(2);
		return str;
	}

	public String getMessage(String strMessage) {
		int intMessageLength = strMessage.length();
		int intMessageCounter = 0;
		String strMessageCharacter = "", strReturnValue = "";
		while (intMessageCounter < intMessageLength) {
			strMessageCharacter = strMessage.charAt(intMessageCounter) + "";
			if (strMessageCharacter.equals("1") || strMessageCharacter.equals("2") || strMessageCharacter.equals("3")
					|| strMessageCharacter.equals("4") || strMessageCharacter.equals("5")
					|| strMessageCharacter.equals("6") || strMessageCharacter.equals("7")
					|| strMessageCharacter.equals("8") || strMessageCharacter.equals("9")
					|| strMessageCharacter.equals("0")) {
				strReturnValue += strMessageCharacter;
			}
			intMessageCounter++;
		}
		if (strReturnValue != null && strReturnValue.length() > 10) {
			strReturnValue = strReturnValue.substring(0, 10);
		}

		return strReturnValue;
	}
}
