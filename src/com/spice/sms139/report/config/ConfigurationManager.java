package com.spice.sms139.report.config;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

public class ConfigurationManager {
	private static ConfigurationManager cm = null;
	private static Properties p = null;
	
	private ConfigurationManager() {
		reload();
	}
	
	public static ConfigurationManager getInstance() {
		if (cm == null) {
			cm = new ConfigurationManager();
		}
		
		return cm;
	}
	
	private void reload() {
		p = new Properties();
		
		String packageName = this.getClass().getPackage().getName() + ".config";
		ResourceBundle bundle = ResourceBundle.getBundle(packageName);
		
		for (String key : bundle.keySet()) {
			p.put(key, bundle.getObject(key));
		}
	}
	
	public Properties getProperties() {
		if (p == null) {
			this.reload();
		}
		
		return p;
	}
	
	public String getProperty(String propertyName) {
		if (p == null) {
			this.reload();
		}
		
		return p.getProperty(propertyName);
	}
	
	public String getProperty(String prefix, String propertyName) {
		if (p == null) {
			this.reload();
		}
		
		return p.getProperty(prefix + "." + propertyName);
	}
	
	public boolean isValid() {
		return p != null && p.size() > 0;
	}
	
	public static void main(String[] args) throws IOException {
		String location = ConfigurationManager.class.getProtectionDomain().getCodeSource().getLocation().getFile();
		
		location = location.substring(0, location.lastIndexOf('/'));
		
		File dir = new File(location);
		
		System.out.println(location);
		System.out.println(dir.getAbsolutePath());
		System.out.println(dir.exists());
	}
}
