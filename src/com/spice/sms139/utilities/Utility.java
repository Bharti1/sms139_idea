package com.spice.sms139.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Utility {

	public static String checkNull(String value) {
		if (value == null) {
			return value = "NA";
		}
		return value;
	}

	private static Properties apiUrlProperties;

	public static String getUrlValue(String key) throws IOException {
		FileInputStream fis = null;
		try {
			String filePath = "";
			if (apiUrlProperties == null) {
				apiUrlProperties = new Properties();
				if (OsType.isWindows()) {
					filePath = "H:/apiUrls.properties";
				} else {
					filePath = "/home/sms139_services/apiUrls.properties";
				}
				fis = new FileInputStream(filePath);
				apiUrlProperties.load(fis);
			}
		} catch (Exception e) {
			System.out.println("Error while loading Property file:" + e.getMessage());
		} finally {
			if (fis != null) {
				fis.close();
			}
		}
		return apiUrlProperties.getProperty(key);
	}

}
