package com.spice.sms139.utilities;

import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;

public class Logs {

	private static synchronized void writeLog(String strSubDir, String logString) {

		String strLogFilePath = "";

		if (OsType.isWindows()) {
			strLogFilePath = "E:/sms139_logs/sms139_idea/";
		} else {
			try {
				strLogFilePath = "/home/sms139_logs/sms139_idea/";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		String strFileName = "";

		String strDateDir = "";

		Calendar objCalendarRightNow = Calendar.getInstance();

		int intMonth = objCalendarRightNow.get(Calendar.MONTH) + 1;

		int intDate = objCalendarRightNow.get(Calendar.DATE);

		int intHour = objCalendarRightNow.get(Calendar.HOUR_OF_DAY);

		int intMinute = objCalendarRightNow.get(Calendar.MINUTE);

		int intSecond = objCalendarRightNow.get(Calendar.SECOND);

		int intMilliSecond = objCalendarRightNow.get(Calendar.MILLISECOND);

		String strYear = "" + objCalendarRightNow.get(Calendar.YEAR);

		strDateDir = strLogFilePath + "/" + intDate + "-" + intMonth + "-" + strYear;
		createDateDir(strDateDir);
		strFileName = strDateDir + "/" + strSubDir + "_" + intHour + ".log";

		try {
			FileWriter out = new FileWriter(strFileName, true);

			String strLogString = intDate + "-" + intMonth + "-" + strYear + " " + intHour + ":" + intMinute + ":"
					+ intSecond + ":" + intMilliSecond + "#" + logString + "\n";
			out.write(strLogString);
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(0);
		}

	}

	private synchronized static void createDateDir(String dateDir) {
		try {
			String folderName = dateDir;
			new File(folderName).mkdirs();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void getErrorLogs(String logString) {
		writeLog("SMS139IdeaErrorLog", logString);
	}

	public static void getRequestLogs(String logString) {
		writeLog("SMS139IdeaRequestLog", logString);
	}

	public static void getResponseLogs(String logString) {
		writeLog("SMS139IdeaResponseLog", logString);
	}
	
	public static void getRetryLogs(String logString) {
		writeLog("SMS139IdeaRetryLog", logString);
	}

}